use qatraining;

-- show me the forename, surname, of trainers with
-- the start date and title of the course they taught
select forename, surname, title, startdate
from trainers join events
	on trainers.trainerid = events.trainerid
join courses 
	on events.courseid = courses.courseid;
    
-- show the mysql proc table, procedures are stored in here!
use mysql;
select * from proc;

-- go back to qatraining
use qatraining;
select * from trainers;

-- aggregate functions
select count(*), sum(costperday), avg(costperday) from trainers;
select count(*), sum(costperday), avg(costperday) from trainers where forename = 'Paul';

-- maximum cost
select max(costperday) from trainers;

-- having filters the aggregate field
select venueID, count(*) from events group by venueID having count(*) > 1;

select * from events;


-- reset the proc
drop procedure if exists hello;

-- change the delimiter
delimiter //

-- create the procedure, return hello world, or any sql statement
create procedure hello()
begin
	select 'hello world', now();
end//

-- reset the delimiter
delimiter ;

-- call the procedure
call hello();

-- iterate, shows a loop and an if statement 
delimiter //
CREATE PROCEDURE doiterate(p1 INT) 
BEGIN 
	label1: LOOP 
		SET p1 = p1 + 1; 
		IF p1 < 10 THEN 
			ITERATE label1; 
		END IF; 
		LEAVE label1; 
	END LOOP label1; 
	SET @x = p1; 
END//
delimiter ;

call doiterate(1);
select @x; -- global var set by the procedure

-- proceedure for getting the average cost per day from the trainer table

drop procedure if exists averageCostPerDay;

-- procedure to work out the average cost per day and 
-- then output horray or boo based on the number
delimiter //
create procedure averageCostPerDay()
begin
	set @a = (select avg(costperday) from trainers);
    if @a < 5 then
		select 'horray';
	else 
		select 'boo';
	end if;
end//
delimiter ;

call averageCostPerDay();
select @a;

-- Show the trainers with a cost per day 
-- higher than the average cost
set @b = (select avg(costperday) from trainers);
select * from trainers where costperday > @b;  -- use the variable set in the line above
select * from trainers where costperday > @a;  -- use the variable set in the procedure