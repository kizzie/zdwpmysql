-- drop the database and recreate if it exists
drop database if exists angus_animal;
create database angus_animal;
use angus_animal;

-- animal (animalid, name, species, clientid)
-- client (clientid, name, address)

-- owner table
create table owner (
	clientid int primary key auto_increment,
	name varchar(50),
    address varchar(100)
) engine = innodb;

-- animal table
create table animal (
	animalid int auto_increment primary key not null,
    name varchar(50) not null,
    species enum('dog', 'cat', 'rabbit', 'bird', 'goldfish'),
    clientid int,
    foreign key (clientid) references owner(clientid) on delete cascade -- to ensure referential integrity
) engine = innodb;

-- adoptionrequest
-- adoption request(date, animalid, clientid, approved)
create table adoptionrequest (
	date date not null,
    animalid int not null,
    foreign key (animalid) references animal(animalid),
    clientid int not null,
    foreign key (clientid) references owner(clientid),
    approved boolean,
	primary key (date, animalid, clientid)
) engine = innodb;

-- describe the table to us
desc animal;
desc owner;

-- insert statements
insert into owner values (default, 'bob', '10 downing street');
insert into animal(animalid, name, species) values (default, 'angus', 'rabbit');
insert into animal values (default, 'shadow', 'rabbit', 1); -- has a reference to bob

-- show what is in each table
select * from owner;
select * from animal;

-- insert some more animals
insert into animal values (5, 'honey', 'dog', 1); -- by using a value it changes the auto_increment counter
insert into animal values (default, 'midas', 'cat', 1);

-- stop SQL from hand holding. If this is set to 1 then we can't update animals 
-- without referencing the primary key and we can't accidently drop the database!
-- set to 0 for this exercise
SET SQL_SAFE_UPDATES = 0;

-- change the owner of Angus to be Kat
insert into owner values (default, 'kat', 'lovely birmingham');
update animal set clientid = 2 where name = 'angus';

-- remove owner 1, should cascade if we use innodb
delete from owner where clientid = 1;

-- TRANSACTIONS --
-- stop auto committing after each statement
set autocommit = 0;

-- begin a transaction
start transaction;

insert into animal values (default, 'NEW ANIMAL', 'rabbit', null);
select * from animal;  -- here the table has one more record
        
-- commit; -- if you want to save it
rollback; -- to roll back to before the transaction

select * from animal; -- no more new animal

-- turn auto committing back on
set autocommit = 1;

select * from animal;
select * from owner;

show databases;
use angus_animal;
update animal set name = 'bob', species = 'dog' where animalid = 1;
select * from animal;