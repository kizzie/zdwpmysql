use qatraining;

-- display all from venues
select * from venues;

-- change the venue cost 
update venues set costperday = 10 where venueid = 2;

-- left join and right join then unioned
select * from trainers as t left join venues v on t.homelocation = v.venueid
union
select * from trainers as t right join venues v on t.homelocation = v.venueid
;

-- self joining
select t1.forename, t1.surname , t2.forename, t2.surname
from trainers t1 
join trainers t2 
on t1.trainerid = t2.manager
; 

-- unioning unrelated things
select * from clients union select forename, surname from trainers;

-- cross join, sql '89 syntax
SELECT * FROM TRAINERS, VENUES WHERE TRAINERS.HOMELOCATION = VENUES.VenueID;

