-- create a view called phonebook
create view phonebook as 
	select forename, surname, telephone from trainers;

select * from phonebook;

-- works as the primary key will auto increment
insert into phonebook values ('kicking', 'king', '234567');

select * from trainers;

desc courses;

-- another view
create view webpagepeople as 
	select title, duration from courses;
    
select * from webpagepeople;

-- causes a warning as we don't have all the fields
insert into webpagepeople value ('kats amazing course', 15);

select * from webpagepeople;
select * from courses;

-- show all the tables and views
show full tables in qatraining;
show tables;