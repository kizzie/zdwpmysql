-- show indexes  for tables
show index from trainers;
show index from eventdelegates;
show index from events;

-- show the status of the engine
show engine innodb status;

-- explaining queries
explain select * from trainers;
explain select * from trainers where forename like '%kat%';
explain select eventid, delegateid from eventdelegates ed1 
where paying = (
	select max(paying) from eventdelegates ed2 where ed1.eventid = ed2.eventid group by eventid
);

-- alter a table to add a new index
ALTER TABLE events ADD INDEX (startdate);