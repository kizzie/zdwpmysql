-- show current users
select * from mysql.user;

-- create a new user and give them access to select things from qatraining
drop user nelly;
create user 'nelly'@'localhost' identified by 'nellypassword';
grant select on qatraining.* to nelly;

show databases;

-- show the current access for nelly or root
show grants for nelly;
show grants for 'root'@'localhost';