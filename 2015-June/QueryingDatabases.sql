use qatraining;

-- selecting specific collumns
select duration, coursecode, title from courses;

-- concatonating the forename and surname
select concat(forename,' ', surname) as name, 
	costperday*2 as double_cost, costperday 
from trainers;

-- use the date function to quarter of the start date
select startdate, quarter(startdate) from events;

-- substring function returns character 2 and 3
select substring(forename,2,3) from trainers;

-- order by, asc by default
select * from trainers order by forename, costperday;

-- selecting and filtering with where
select forename, surname 
from trainers
where forename = 'kat'
or forename = 'david'
or forename = 'philip';

-- filter with the in() statement
select forename, surname 
from trainers
where forename in ('kat', 'david','philip', 'adrian');

select forename, surname, costperday 
from trainers
where costperday between 5 and 10;

-- compare against null
select * from eventdelegates where paying is null;

-- select two trainers
select * from trainers where surname in ('mcivor', 'walker');

-- add 7 to the start date
select date_add(startdate, interval 7 day) from events;
