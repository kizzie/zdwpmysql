-- average cost per course

-- step one, work out the total cost per course
-- step two, work out the total number of courses
-- step three, work out the average
-- won't work without subqueries!
select courses.title, 
	(courses.duration * (venues.costperday + trainers.CostPerDay)) * count(events.courseID) as 'total cost',
    count(events.courseID) as 'count',
    ((courses.duration * (venues.costperday + trainers.CostPerDay)) * count(events.courseID)) / count(events.courseID) as 'average'
from courses join events on courses.CourseID = events.CourseID
join venues on venues.VenueID = events.VenueID
join trainers on trainers.trainerID = events.trainerID
group by courses.courseID;





-- complicated, join two subqueries
select t1.title, t1.courseid, sum(cost), count, sum(cost) / count
from (
	select 	courses.title, courses.CourseID,
			(courses.duration * (venues.costperday + trainers.CostPerDay)) 
as 'cost'
	from courses join events on courses.CourseID = events.CourseID
	join venues on venues.VenueID = events.VenueID
	join trainers on trainers.trainerID = events.trainerID
	) as t1
join (
	select events.courseID, count(events.courseID) as 'count' 
    from events 
    group by events.CourseID
    ) as t2 
on t2.courseID= t1.courseID
group by t1.courseid;


-- better - just use one and use the avg() function
select t1.title, t1.courseid, sum(cost), avg(cost)
from (
	select 	courses.title, courses.CourseID,
			(courses.duration * (venues.costperday + trainers.CostPerDay)) 
            as 'cost'
	from courses join events on courses.CourseID = events.CourseID
	join venues on venues.VenueID = events.VenueID
	join trainers on trainers.trainerID = events.trainerID
	) as t1

group by t1.courseid;