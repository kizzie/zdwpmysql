-- drop existing database
DROP DATABASE if exists qatraining;

-- create new one
CREATE DATABASE qatraining;

use qatraining;

-- TABLES

CREATE TABLE Clients(
	ClientID int not null primary key AUTO_INCREMENT,
    CompanyName varchar(50) not null
) Engine=InnoDB;

create table Courses (
	CourseID int primary key not null AUTO_INCREMENT,
    CourseCode varchar(15) not null,
    Title varchar(200) not null,
    Duration int not null
) Engine=InnoDB;

create table Delegates (
	DelegateID int primary key not null AUTO_INCREMENT,
    ClientID int not null,
    Forename varchar(15) not null,
    Surname varchar(15) not null,
    foreign key (ClientID) references Clients(ClientID)
) Engine=InnoDB;

create table Venues (
	VenueID int primary key not null AUTO_INCREMENT,
    VenueName varchar(50) not null,
    CostPerDay double
) Engine=InnoDB;


create table Trainers (
	TrainerID int primary key not null AUTO_INCREMENT,
    Forename varchar(15) not null,
    Surname varchar(15) not null,
    HomeLocation int, -- left to allow nulls deliberately to use coalesce
    CostPerDay double, 
	Manager int,
    Telephone varchar(15),
    foreign key (HomeLocation) references Venues(VenueID),
    foreign key (Manager) references Trainers(TrainerID),
    index(homelocation, forename, surname)
) Engine=InnoDB;


create table Events (
	EventID int primary key not null AUTO_INCREMENT,
    CourseID int not null,
    VenueID int not null,
    TrainerID int not null,
    StartDate date not null,
    foreign key (CourseID) references Courses(CourseID),
    foreign key (VenueID) references Venues(VenueID),
    foreign key (TrainerID) references Trainers(TrainerID)
) Engine=InnoDB;

create table EventDelegates (
	EventID int not null,
    DelegateID int not null,
    primary key (EventID, DelegateID),
    foreign key (EventID) references Events(EventID),
    foreign key (DelegateID) references Delegates(DelegateID),
    Paying double
) Engine=InnoDB;


/**** adding the values *** */
  Insert into clients values (default, 'Letter land');
  insert into clients values (default, 'Hogwarts');
  
  INSERT INTO venues VALUES (default, 'King William Street, London', 100);
  INSERT INTO venues VALUES (default, 'Brown lane West, Leeds', 50);
  INSERT INTO venues VALUES (default, 'Russell Street, Leeds', 50);
  INSERT INTO venues VALUES (default, 'Middlesex Street, London', 95);
  INSERT INTO venues VALUES (default, 'Tabernacle Street, London', 45);
  INSERT INTO venues VALUES (default, 'Rosebery Avenue, London', 25);
  INSERT INTO venues VALUES (default, 'South Gyle, Edinburgh', 90);
  
  INSERT INTO trainers VALUES (default, 'Philip', 'Stirpe', 2, 10, null, '1234');
  INSERT INTO trainers values (default, 'Adrian', 'Jakeman', 1, 10, null, '12345');
  INSERT INTO trainers values (default, 'Paul', 'Mercer', 4, 5, 2, null);
  INSERT INTO trainers values (default, 'David', 'Walker', 7, 10, 2, '234');
  INSERT INTO trainers values (default, 'Matt', 'Bishop', 7, 5, null, '345');
  INSERT INTO trainers values (default, 'Daniel', 'Ives', 3, 3, 5, '456');
  INSERT INTO trainers values (default, 'Paul', 'Besly', 1, 7, 5, '567');
  insert into trainers values (default, 'Kat', 'McIvor', 4, 20, null, null);
  insert into trainers values (default, 'A.N.', 'Trainer', null, null, null, null);
     
           
 INSERT INTO courses VALUES (default, 'QAWCF10', 'Building Effective Windows Communication Foundation applications using Visual Studio 2010', 3);
 INSERT INTO courses VALUES (default, 'QAWPF4', 'Building Effective Windows Presentation Foundation applications using Visual Studio 2010 and Expression Blend 4', 5);
 INSERT INTO courses VALUES (default, 'QAASPMVC-3', 'Building Effective ASP.NET MVC 3.0 Web Sites using Visual Studio 2010', 5);
 INSERT INTO courses VALUES (default, 'QAJQUERY', 'Leveraging the Power of jQuery', 2);
 INSERT INTO courses VALUES (default, 'QAEDNF4', 'Enterprise Microsoft .NET 4.0 Framework', 5);
 INSERT INTO courses VALUES (default, 'QACLOUD-1', 'Transitioning to the Cloud with the Azure Services Platform', 1);
	
 insert into delegates values (default, 1, 'Annie', 'Apple');
 insert into delegates values (default, 1, 'Bouncy', 'Ben');
 insert into delegates values (default, 1, 'Clever', 'Cat');
 insert into delegates values (default, 1, 'Dippy', 'Duck');
 insert into delegates values (default, 1, 'Eddie', 'Elephant');
 
 insert into delegates values (default, 2, 'Harry', 'Potter');
 insert into delegates values (default, 2, 'Ron', 'Wesley');
 insert into delegates values (default, 2, 'Malfoy', 'Draco');
 insert into delegates values (default, 2, 'Hermione', 'Granger');
 insert into delegates values (default, 2, 'Annie', 'Other');
 
 
 insert into events values (default, 1, 2, 1, '2014-1-1');
 insert into events values (default, 6, 1, 2, '2013-2-15');
 insert into events values (default, 3, 5, 3,'2015-3-20');
 insert into events values (default, 2, 2, 4,'2015-3-20');
 insert into events values (default, 1, 2, 4,'2015-9-30');
 insert into events values (default, 2, 7, 8,'2015-12-12');
 
 insert into eventdelegates values (1, 1, 50);
 insert into eventdelegates values (1, 2, 50);
 insert into eventdelegates values (1, 3, 50);
 insert into eventdelegates values (1, 7, 50);
 insert into eventdelegates values (1, 8, 50);
 
 insert into eventdelegates values (2, 8, 100);
 insert into eventdelegates values (2, 9, 200);
 insert into eventdelegates values (2, 1, 100);
 insert into eventdelegates values (2, 10, 50);
 
 insert into eventdelegates values (6, 1, null);
 insert into eventdelegates values (6, 2, null);
 insert into eventdelegates values (6, 3, null);
 insert into eventdelegates values (6, 10, null);
 
 
 -- test query to see if the links are working
 select events.EventID,
		CourseCode, 	
		VenueName, 
        StartDate,
        Concat(Trainers.Forename, ' ', Trainers.Surname) as 'Trainer Name',
        Concat(Delegates.Forename, ' ', Delegates.Surname) as 'Delegate Name',
        clients.CompanyName
      --  (trainers.CostPerDay + venues.CostPerDay) * courses.Duration as 'Total Cost',
      --  sum(eventdelegates.Paying) as 'Total Incoming'
	from events 
	join courses on events.CourseID = courses.CourseID
	join venues on events.VenueID = venues.VenueID
    join trainers on events.TrainerID = trainers.TrainerID
    join eventdelegates on events.EventID = eventdelegates.EventID
    join delegates on eventdelegates.DelegateID = delegates.DelegateID
    join clients on delegates.ClientID = clients.ClientID
	-- group by events.eventID
 
 