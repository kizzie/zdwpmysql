use qatraining;

-- 1
select * from trainers;

-- 2
select forename, surname from trainers;

-- 3
select VenueName, CostPerDay from venues;

-- 4
select * from events where venueid = 2;

-- 5
select * from events where venueid = 2 and courseid =1;

-- 6
select trainerid, substring(forename, 1,1) as initial1, substring(surname,1,1) as initial2 from trainers;

-- 7
select trainerid, concat(substring(forename, 1,1), '.',substring(surname,1,1)) as initials from trainers;

-- 8
select distinct homelocation from trainers;

-- 9
select * from eventdelegates where paying is null;

-- 10
select * from eventdelegates where paying is null order by delegateID desc;

-- 11
select * from trainers order by costperday desc;

-- 12
select * from events where StartDate between '2015-03-01' and '2015-09-01';

-- 13
select month(startdate) as 'month' from events;
