-- 1
select *
from trainers 
where homelocation in (
	select venueid from venues 
	where venuename like '%london%'
);

-- 2
select * from trainers where trainerid in (
	select trainerid from events
);

-- a more complex solution that does the same!
select * from trainers t1 where exists (
	select * from events where t1.trainerid = events.trainerid
);

-- 3
select venueid, venuename, costperday from venues where costperday = (
	select max(costperday) from venues
);

-- 4
select eventid, delegateid from eventdelegates ed1 
where paying = (
	select max(paying) from eventdelegates ed2 where ed1.eventid = ed2.eventid group by eventid
);
