-- 1	Calculate the income from each event being run. 
-- To do this we need to calculate how much is being paid by each of the delegates
use qatraining;

select events.eventid, sum(paying)
from events 
	join eventdelegates on events.EventID = eventdelegates.EventID
group by eventdelegates.eventid;

-- 2.	Calculate the overall cost / income from each event. 
-- We did half of this in part 1, the other half in the previous exercise 
-- (working out the trainer cost + the venue cost) to give an overall figure for the event

select 	events.eventid, 
coalesce(sum(paying), 0) as 'Total In', 
(trainers.CostPerDay + venues.CostPerDay) as 'Total Out', 
coalesce(sum(paying),0) - (trainers.CostPerDay + venues.CostPerDay) as 'Overall'
from events 
	join eventdelegates on events.EventID = eventdelegates.EventID
    join venues on venues.VenueID = events.VenueID
    join trainers on trainers.TrainerID = events.TrainerID
group by eventdelegates.eventid;

-- 3.	Calculate the income from delegates for each of the venues

select venues.venueID, sum(paying) 
from events 
    join eventdelegates on events.eventID = eventdelegates.eventID
    join venues on events.venueID = venues.venueID
group by events.venueID;

-- 4.	Calculate the number of delegates and the average amount paid for each venue

select venues.venueID, sum(paying), count(paying), avg(paying)
from events 
    join eventdelegates on events.eventID = eventdelegates.eventID
    join venues on events.venueID = venues.venueID
group by events.venueID;

-- 5.	Instead of listing the VenueID from part four, change it so that the venue name is listed. 
-- Tidy up the nulls by using coalesce.

select venues.VenueName, 
coalesce(sum(paying), 0) as 'Total In', 
coalesce(count(paying), 0) as 'Total Paid', 
-- coalesce(count(delegateID), 0) as 'Number actually attending', 
coalesce(avg(paying), 0) as 'Average Paid'
from events 
    join eventdelegates on events.eventID = eventdelegates.eventID
    join venues on events.venueID = venues.venueID
group by events.venueID;
 
-- 6
select venues.VenueName, 
coalesce(sum(paying), 0) as 'Total In', 
coalesce(count(paying), 0) as 'Total Paid', 
coalesce(count(delegateID), 0) as 'Number actually attending', 
coalesce(avg(paying), 0) as 'Average Paid'
from events 
    join eventdelegates on events.eventID = eventdelegates.eventID
    join venues on events.venueID = venues.venueID
group by events.venueID;

-- 7.	Show the number of courses taught by each trainer 
-- where the number of events is greater than or equal to 2.

select forename, surname, count(events.eventID) 
from trainers
	join events on trainers.trainerID = events.trainerID
group by events.trainerID
having count(events.eventID) >= 2;

-- 8 

select events.eventid, clientid, sum(paying) 
from events 
	join eventdelegates on events.eventid = eventdelegates.eventid 
	join delegates on eventdelegates.delegateid = delegates.delegateid
group by eventid, ClientID

-- 9 proceedures

delimiter //

create procedure helloworld() 
begin
	select now(), 'Kat';
end //
delimiter ;

call helloworld();

-- 10

delimiter //
create procedure usingif() 
  begin 
   declare msg varchar(40);
   set @day = dayofweek(now());

   if @day = 2 then
    set msg = 'I don\'t care if Monday\'s blue';
   elseif @day = 3 then
    set msg = 'Tuesday\'s grey';
   elseif @day = 4 then
    set msg = 'and Wednesday too';
   elseif @day = 5 then
    set msg = 'Thursday I don\'t care about you';
   elseif @day = 6 then
    set msg = 'Friday I\'m in love';
   elseif @day = 7 then
    set msg = 'Saturday wait';
   elseif @day = 8 then
    set msg = 'And Sunday always comes too late';
   end if; 
    
   select msg;
end //
delimiter ;

call usingif();

-- 11

delimiter //
create procedure usingifWithInput(in day int) 
  begin 
   declare msg varchar(40);
   -- set @day = dayofweek(now());

   if day = 2 then
    set msg = 'I don\'t care if Monday\'s blue';
   elseif day = 3 then
    set msg = 'Tuesday\'s grey';
   elseif day = 4 then
    set msg = 'and Wednesday too';
   elseif day = 5 then
    set msg = 'Thursday I don\'t care about you';
   elseif day = 6 then
    set msg = 'Friday I\'m in love';
   elseif day = 7 then
    set msg = 'Saturday wait';
   elseif day = 1 then
    set msg = 'And Sunday always comes too late';
   end if; 
    
   select msg;
end //
delimiter ;


call usingifWithInput(2);
call usingifWithInput(3);
call usingifWithInput(4);
call usingifWithInput(5);
call usingifWithInput(6);
call usingifWithInput(7);
call usingifWithInput(1);