

-- 1 
create view phonebook1 as ( 
  select t1.forename, t1.surname, t1.telephone
  from trainers t1 
  order by surname 
);

-- 2
create view phonebook as ( 
  select t1.forename, t1.surname, t1.telephone, concat(t2.forename, ' ', t2.surname) as manager
  from trainers t1 left join trainers t2 
  on t1.manager = t2.trainerid
  order by surname 
);

-- 3 

create view londontrainers as (
  select trainers.*
  from trainers 
  where homelocation in (
    select venueid from venues 
    where venuename like '%london%'
  )
) with check option;

select * from londontrainers;

insert into londontrainers values (default, 'new', 'new', 1, 9000, null, '0000');
insert into londontrainers values (default, 'new', 'new', 2, 9000, null, '0000');

-- 4 
-- Create the two tables
use qatraining;
drop table if exists trainer_company;
drop table if exists trainer_personal;

create table trainer_personal (
  trainerid int primary key auto_increment,
  forename varchar(20),
  surname varchar(20),
  telephone varchar(20)
);


create table trainer_company (
trainerid int primary key auto_increment,
homelocation int,
costperday int,
manager int,
foreign key (homelocation) references venues(venueid),
foreign key (manager) references trainer_personal(trainerid)
);
-- populate them

insert into trainer_personal (
	select trainerid, forename, surname, telephone from trainers
);

insert into trainer_company (
	select trainerid, homelocation, costperday, manager from trainers
);
    

-- create a trainer view
drop view trainers_view;
create view trainers_view as (
	select trainer_personal.*, homelocation, costperday, manager
    from trainer_personal 
    join trainer_company 
    on trainer_personal.trainerid = trainer_company.trainerid
);

select * from trainers_view;
select * from trainers;