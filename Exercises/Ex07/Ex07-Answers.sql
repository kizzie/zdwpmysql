-- 1.	Select the Company Name, Delegate Forename and Surname to produce the following output:

select CompanyName, Forename, Surname from clients join delegates on clients.clientid = delegates.clientid;

-- 2 Output the name of the home location for each of the trainers

select forename, surname, VenueName from trainers join venues on trainers.homelocation = venues.VenueID;

-- 3.	Look at the event table. There are several foreign keys there. Create a table that Holds the 
-- Course Name, Start Date, Length of the course, and the trainer name 

select title, StartDate, Duration, forename, surname 
from events 
	join courses on events.courseid = courses.courseid
    join trainers on events.trainerid = trainers.trainerid;

-- 4. Add a collumn to the table produced in part 3 which calculates the cost of the course
-- it is the venue cost plus the trainer cost
 
select title, StartDate, Duration, forename, surname, 
venues.CostPerDay + trainers.CostPerDay as 'total cost'
from events 
	join courses on events.courseid = courses.courseid
    join trainers on events.trainerid = trainers.trainerid
    join venues on events.venueId = venues.venueID;

-- 5 Join the trainer table to itself to get a list of the trainers and their managers. 
-- Concatonate the names together to make two collumns, trainer name and manager name

select concat(t1.forename, ' ', t1.surname) as trainer, concat(t2.forename,' ', t2.surname) as manager from trainers t1 join trainers t2 on t1.manager = t2.trainerid;

-- 6 Do the same join for a left and a right join. See the difference
select concat(t1.forename, ' ', t1.surname) as trainer, concat(t2.forename,' ', t2.surname) as manager from trainers t1 left join trainers t2 on t1.manager = t2.trainerid;
select concat(t1.forename, ' ', t1.surname) as trainer, concat(t2.forename,' ', t2.surname) as manager from trainers t1 right join trainers t2 on t1.manager = t2.trainerid;

-- 7 There is a function called 'coalesce' that we can use when dealing with null values. It either outputs
-- the contents of a field, or an alternative value. 
-- For example: coaslesce (manager, "no manager found") would either output the manager id or the string value
-- create a table which performs a left join between the trainer table to itself and outputs 
-- the name of the trainer with the surname of the manager or 'no manager found' if there is not one
-- Order it by the manager field

select concat(t1.forename, ' ', t1.surname) as Trainer, coalesce(t2.surname, "No manager found") as Manager
from trainers as t1 
	left join trainers as t2 on t1.manager = t2.trainerid
    order by manager;