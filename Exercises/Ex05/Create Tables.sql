
-- drop existing database
DROP DATABASE if exists qatraining;

-- create new one
CREATE DATABASE qatraining;

use qatraining;

-- TABLES

CREATE TABLE Clients(
	ClientID int not null primary key AUTO_INCREMENT,
    CompanyName varchar(50) not null
) Engine=InnoDB;

create table Courses (
	CourseID int primary key not null AUTO_INCREMENT,
    CourseCode varchar(15) not null,
    Title varchar(200) not null,
    Duration int not null
) Engine=InnoDB;

create table Delegates (
    DelegateID int primary key not null AUTO_INCREMENT,
    ClientID int not null,
    Forename varchar(15) not null,
    Surname varchar(15) not null,
    foreign key (ClientID) references Clients(ClientID)
) Engine=InnoDB;

create table Venues (
	VenueID int primary key not null AUTO_INCREMENT,
    VenueName varchar(50) not null,
    CostPerDay double
) Engine=InnoDB;


create table Trainers (
	TrainerID int primary key not null AUTO_INCREMENT,
    Forename varchar(15) not null,
    Surname varchar(15) not null,
    HomeLocation int, -- left to allow nulls deliberately to use coalesce
    CostPerDay double, 
    foreign key (HomeLocation) references Venues(VenueID)
) Engine=InnoDB;


create table Events (
	EventID int primary key not null AUTO_INCREMENT,
    CourseID int not null,
    VenueID int not null,
    TrainerID int not null,
    StartDate date not null,
    foreign key (CourseID) references Courses(CourseID),
    foreign key (VenueID) references Venues(VenueID),
    foreign key (TrainerID) references Trainers(TrainerID)
) Engine=InnoDB;

create table EventDelegates (
	EventID int not null,
    DelegateID int not null,
    primary key (EventID, DelegateID),
    foreign key (EventID) references Events(EventID),
    foreign key (DelegateID) references Delegates(DelegateID),
    Paying double
) Engine=InnoDB;

