use qatraining;

/**** adding the values *** */
  Insert into Clients values (default, 'Letter land');
  insert into Clients values (default, 'Hogwarts');
  
  INSERT INTO Venues VALUES (default, 'King William Street, London', 100);
  INSERT INTO Venues VALUES (default, 'Brown lane West, Leeds', 50);
  INSERT INTO Venues VALUES (default, 'Russell Street, Leeds', 50);
  INSERT INTO Venues VALUES (default, 'Middlesex Street, London', 95);
  INSERT INTO Venues VALUES (default, 'Tabernacle Street, London', 45);
  INSERT INTO Venues VALUES (default, 'Rosebery Avenue, London', 25);
  INSERT INTO Venues VALUES (default, 'South Gyle, Edinburgh', 90);
  
  INSERT INTO Trainers VALUES (default, 'Philip', 'Stirpe', 2, 10);
  INSERT INTO Trainers values (default, 'Adrian', 'Jakeman', 1, 10);
  INSERT INTO Trainers values (default, 'Paul', 'Mercer', 4, 5);
  INSERT INTO Trainers values (default, 'David', 'Walker', 7, 10);
  INSERT INTO Trainers values (default, 'Matt', 'Bishop', 7, 5);
  INSERT INTO Trainers values (default, 'Daniel', 'Ives', 3, 3);
  INSERT INTO Trainers values (default, 'Paul', 'Besly', 1, 7);
  insert into Trainers values (default, 'Kat', 'McIvor', 4, 20);
     
           
 INSERT INTO Courses VALUES (default, 'QAWCF10', 'Building Effective Windows Communication Foundation applications using Visual Studio 2010', 3);
 INSERT INTO Courses VALUES (default, 'QAWPF4', 'Building Effective Windows Presentation Foundation applications using Visual Studio 2010 and Expression Blend 4', 5);
 INSERT INTO Courses VALUES (default, 'QAASPMVC-3', 'Building Effective ASP.NET MVC 3.0 Web Sites using Visual Studio 2010', 5);
 INSERT INTO Courses VALUES (default, 'QAJQUERY', 'Leveraging the Power of jQuery', 2);
 INSERT INTO Courses VALUES (default, 'QAEDNF4', 'Enterprise Microsoft .NET 4.0 Framework', 5);
 INSERT INTO Courses VALUES (default, 'QACLOUD-1', 'Transitioning to the Cloud with the Azure Services Platform', 1);
    
 insert into Delegates values (default, 1, 'Annie', 'Apple');
 insert into Delegates values (default, 1, 'Bouncy', 'Ben');
 insert into Delegates values (default, 1, 'Clever', 'Cat');
 insert into Delegates values (default, 1, 'Dippy', 'Duck');
 insert into Delegates values (default, 1, 'Eddie', 'Elephant');

 insert into Delegates values (default, 2, 'Harry', 'Potter');
 insert into Delegates values (default, 2, 'Ron', 'Wesley');
 insert into Delegates values (default, 2, 'Malfoy', 'Draco');
 insert into Delegates values (default, 2, 'Hermione', 'Granger');
 insert into Delegates values (default, 2, 'Annie', 'Other');
 
 
 insert into Events values (default, 1, 2, 1, '2014-1-1');
 insert into Events values (default, 6, 1, 2, '2013-2-15');
 insert into Events values (default, 3, 5, 3,'2015-3-20');
 insert into Events values (default, 2, 2, 4,'2015-3-20');
 insert into Events values (default, 1, 2, 4,'2015-9-30');
 insert into Events values (default, 2, 7, 8,'2015-12-12');
 
 insert into EventDelegates values (1, 1, 50);
 insert into EventDelegates values (1, 2, 50);
 insert into EventDelegates values (1, 3, 50);
 insert into EventDelegates values (1, 7, 50);
 insert into EventDelegates values (1, 8, 50);

 insert into EventDelegates values (2, 8, 100);
 insert into EventDelegates values (2, 9, 200);
 insert into EventDelegates values (2, 1, 100);
 insert into EventDelegates values (2, 10, 50);

 insert into EventDelegates values (6, 1, null);
 insert into EventDelegates values (6, 2, null);
 insert into EventDelegates values (6, 3, null);
 insert into EventDelegates values (6, 10, null);
