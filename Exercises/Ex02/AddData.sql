use Ex01;

/*
If using safe update mode then we need to tell it that it we are okay to delete things...
To disable safe mode, toggle the option in Preferences -> SQL Editor and reconnect
*/
delete from sale;
delete from salesperson;
delete from contact;
delete from dept;
delete from company;


insert into dept values ( 1,'Animal Products',	'Adam Apricot', 5);
insert into dept values ( 2,'Business Systems',	'Barbara Banana',15 );
insert into dept values ( 3,'Credit Control',	'Paul Peach', 	25 );
insert into dept values ( 4,'Desktop Systems',	'Diver Dan', 	5 );
insert into dept values ( 5,'Electrical Repairs','Xavier Xylophone',45 );

insert  into salesperson values( 10, 'Alan', 	'Brick', 	1,  9, 	'Surrey', 	'RT8 8LP', '0207-908-8765', 	'Joined in Sept 1996' );
insert  into salesperson values( 20, 'Billy', 	'Custard', 	2, 14,	'Hampshire', 	'RF3 9UD', '(0306)7871',        'Bright Prospect' );
insert  into salesperson values( 30, 'Chris', 	'Digger', 	2,  7, 	'Hampshire',	'W45 TY3', '(0908)922211', 	'Should be promoted within the next 4 months' );
insert  into salesperson values( 40, 'Dick', 	'Ernst', 	3, 11, 	'London', 	NULL, 	   '0207-898-9656', 	NULL );
insert  into salesperson values( 50, 'Ernest',  'Flipper', 	3, 12, 	'Surrey', 	'CR1 2GH', '0784-898116', 	NULL );
insert  into salesperson values( 60, 'Fred', 	'Goalie', 	3, 13, 	'Surrey', 	NULL, 	   '0207-877-0123', 	'Dislikes international travel' );


insert  into company values( 1000, 'Happy Heaters PLC',	  	'(01306)345672',	'London', 'SE3 89L' );
insert  into company values( 2000, 'Icicle Igloos Inc',  	'0207-987-1265', 	'London' , 'N1 4LH' );
insert  into company values( 3000, 'Judo Jeans PLC', 		'0207-478-2990', 	'London', 'N9 2FG' );
insert  into company values( 4000, 'Kipper Kickers Inc',  	'01254-987766', 	'Devon', 'PL4 9RT' );

insert into contact values( 1000, 'MM', 'Munching Mike', 'Accounts Officer', '(0207)223-9887', 'We first visited her in January 2007' );
insert into contact values( 2000, 'NN', 'Naughty Nick', 'Bought Ledger Manager', '01546-456566 Ext 22', 'Works only on Monday and Wednesdays' );
insert into contact values( 2000, 'OO', 'Ollie Octopus', 'Chief Executive Officer', '0207-341-566670 ext 10', '' );
insert into contact values( 3000, 'PP', 'Purposeful Peter', 'Development Director', '0131 324545 ext 213', 'Insists on personally signing all orders' );
insert into contact values( 3000, 'QQ', 'Quentin Quail', 'Electrical Manager', '01456 802071 ext 44', 'Has been in his job a long time' );
insert into contact values( 3000, 'RR', 'Robber Red', 'Federal Reporting Officer', '0356-345345', 'Has a preference for Apple Macs' );
insert into contact values( 4000, 'RR', 'Ricky Rambo', 'Gourmet Foods Purchaser', '0207-988-0777', '' );
insert into contact values( 4000, 'TT', 'Terrible Tim', 'Head of Inter Office Systems', '05673-476878 ext 221', 'Is listed in Whos Who 2005 onwards' );
insert into contact values( 4000, 'UU', 'Uppy Umbrella', 'Accounts Officer', '0823-598494 ext 1', 'Is upset because he is not chief executive!' );


insert into sale values( 100, 60,     1000, 'MM',   7.00,  '2006-06-24', 'Toshiba 6700 Pro' );
insert into sale values( 200, 60,     3000, 'QQ',   6.00, '2006-05-01', 'MS Office Professional * 30' );
insert into sale values( 300, 60,     2000, 'OO',  12.00,   '2006-07-14', 'ScanPRO 4800 Scanner' );
insert into sale values( 400, 10,     1000, 'MM',   5.00,  '2006-09-08', 'Modems and Cables etc' );
insert into sale values( 500, 60,     4000, 'TT',   2.00,  '2006-07-23', 'Laser printer' );
insert into sale values( 600, 50,     3000, 'PP',  27.00, '2006-05-23', 'Complete Desktop Publishing System' );
insert into sale values( 700, 10,     2000, 'OO',   3.00,   '2006-01-23', 'SQL Server 2007 20 user licence' );
insert into sale values( 800, 60,     3000, 'RR',   3.00,   '2006-11-15', 'Printer cartridges' );


