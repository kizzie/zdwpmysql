<html>
	<head>
		<title>Search for trainers</title>
	</head>

	<body>
		<h1>Please enter the trainer name or surname</h1>
		<!-- Step 1 -->
		<form action = "index.php" method = "get">
			Search Term: <input type = "text" name = "term" /> <br />
			<input type = "submit" name = "submit"/>
		</form>

		<!-- Step 3 -->
		<?php
			// if the form has been submitted
			if (isset($_GET['submit'])){
				//4: create a database connection
				$db =  new PDO('mysql:host=localhost;dbname=qatraining', 'root', '');
				//5: Get the values submitted from the form. Adding % around for the LIKE query
				//and using quote to remove SQL injection attack issues
				$safe_term = $db->quote('%'.$_GET['term'].'%');
				
				//6: The query to run
				$query = "select * from trainers where forename like $safe_term or surname like $safe_term;";
				
				//7: Run the query
				$result = $db->query($query);
			
				//8: output the result
				foreach ($result as $row) {
					echo "<h1>{$row['TrainerID']}: {$row['Forename']} {$row['Surname']}</h1>
						<p> Home Location: {$row['HomeLocation']} </p>
						<p> Cost per day: {$row['CostPerDay']} </p>
						<p> Manager: {$row['Manager']} </p>	
						<p> Telephone: {$row['Telephone']} </p>
					";
				}

				//9: If there was no trainer, add an error message
				if ($result->rowCount() == 0) {
					echo "<p>No trainer with that name found</p>";
				}
			}
		?>
	</body>
</html>
