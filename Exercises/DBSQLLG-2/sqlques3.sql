/***************************************************************************************
**										      **
** 		CHAPTER 3. Basic Data Manipulation Practical 1.		  	      **
**		Limiting columns, calculations, aliases, sorting and duplicates	      **
** 										      **
****************************************************************************************/
SELECT * FROM salesperson
SELECT * FROM sale

--p1) Look at your supplied 'schema'. 
--    Choose ONE of those 5 tables and write the FROM clause.
--    Focus your eyes on the column names of that table.
--    Now Select any THREE columns of your choice from that table.
--    Your code should resemble this structure:

--    SELECT columnA, columnB, columnC
--    FROM tableZ

--    You decide what tableZ, columnA, columnB, columnC are!!


 
--p2) We would like you to list all the salespeople, so write the FROM clause now.

--    Some INFORMATION for you - the sales_targets of the salespeople are YEARLY targets.  
--    We would like you to display (SELECT) 2 columns only.
--    Please display the SURNAMES and the QUARTERLY target of each salesperson.
--    One of the 2 columns you have just selected is a Calculated Column.
--    This column should have a heading (ALIAS) of 'Quarterly Target'. 
--    We would also like the rows to be sorted by these quarterly targets, 
--    but with the largest value appearing first.





lname           Quarterly Target   
--------------- ------------------ 
Custard         3.500000
Goalie          3.250000
Flipper         3.000000
Ernst           2.750000
Brick           2.250000
Digger          1.750000


--p3) Display 3 columns, each salesperson's employee number, dept number and initials.
--    The initials are not in the table but can be retrieved from the firstname and lastname.
--    They should be displayed as one value separated by a fullstop, eg. Alan Brick should be �A.B�.
--    The answer set should have a column heading saying 'Initials'.
--    REMINDER, the function 'SUBSTRING' has EXACTLY 3 MANDATORY parameters.




emp_no      dept_no     Initials 
----------- ----------- -------- 
10          1           A.B
20          2           B.C
30          2           C.D
40          3           D.E
50          3           E.F
60          3           F.G


--p4) List the employee numbers of those salespeople who have sold. 
--    Remove duplicates!! - (3 row result set)



emp_no      
----------- 
10
50
60

-- IF YOU HAVE TIME

--p5) From the salesperson table list the dept_no/county combinations. Again, 
--    no duplicate entries please. - (4 rows)





dept_no     county          
----------- --------------- 
1           Surrey
2           Hampshire
3           London
3           Surrey

--p6) List (sorted) just the month numbers of the 8 sales. 




month       
----------- 
1
5
5
6
7
7
8
11

/***************************************************************************************
**										      **
** 		END OF CHAPTER 3. Practical 1			        	      **
**										      **
****************************************************************************************/






























/***************************************************************************************
**										      **
** 		CHAPTER 3. Basic Data Manipulation Practical 2.		  	      **
**		Using a 'WHERE' clause to restrict rows				      **
**                                                                                    **
****************************************************************************************/

-- Just in case you need them:
SELECT * FROM dept
SELECT * FROM sale
SELECT * FROM contact

-- ******** NOTE
-- If you are not instructed which columns to 'Select' then you choose, or put '*'.
-- This practical is testing your ability to write a 'Where' clause.
-- ******** NOTE


--p1) Display only those sales whose order value is in the range 10 to 30 inclusive 
--    (2 row result set). Note which employee(s) made these 2 sales.





order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
300         60          2000        OO           12          2006-07-14 00:00:00.000                                ScanPRO 4800 Scanner
600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System

--p2) Copy/Paste Part 1 and amend the WHERE clause so that we further restrict these sales 
--    to just those made by either employee 10 or employee 50 (1 row result set).

--    Write the code now here.




order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System


-- Did you just use the word 'OR' in the last bit of code?
-- If you did, and used no parentheses i.e. '(' and  ')', then you asked the the wrong question
-- although you might appear to have the correct answer!!
-- Hopefully you used the 'IN' word which avoided using 'OR'. If you didn�t, ensure you now do.




















-- This SQL is INCORRECT, as it would show all sales by emp_no 50 regardless of value:

SELECT 	* 
FROM	sale
WHERE	order_value BETWEEN 10 AND 30
AND	emp_no = 10 OR emp_no = 50



--p3) Produce firstly a list of departments whose name contains  'SYS'. 
--    Get this working first!! (depts 2 & 4 should appear).

--    Now amend the query to ALSO include any depts whose sales_target is less than 10.
--    Get that working now (depts 1, 2 & 4 should appear).

--    But we DO NOT want to see under ANY circumstances 
--    a row in the answer set that has 'Barbara Banana' as manager.

--    Amend the query further to reflect this requirement (depts 1 & 4 ONLY should now appear).
--    If you have the wrong answer, OR if you have used no parentheses, then revisit your code!!








dept_no     dept_name            manager              sales_target   
----------- -------------------- -------------------- -------------- 
1           Animal Products      Adam Apricot         5.00
4           Desktop Systems      Diver Dan            5.00








-- ONLY IF YOU HAVE TIME

--p4) Display contact names and tel numbers in Inner London.
--    Inner London is '0207'.
--    Note: tel numbers will never be stored as numeric columns because you don't 
--    ever want to do arithmetic on them!
--    (Correct answer is 3 rows, not 2 and not 4)
--    You need to work around some very 'iffy'/'dodgy'/'bad' data.





name                 tel                       
-------------------- ------------------------- 
Munching Mike        (0207)223-9887           
Ollie Octopus        0207-341-566670 ext 10   
Ricky Rambo          0207-988-0777       

-- If you are not happy with your answer, then don't worry, we will REVIEW after practical!

--p5) List sales made in May and July (any year).
--    Use the MS SQL Server date function called 'DATEPART', Shift-F1 for TRANSACT-SQL HELP. 
--    (4 rows)




order_no    order_date                                             
----------- ------------------------------------------------------ 
200         2006-05-01 00:00:00.000
300         2006-07-14 00:00:00.000
500         2006-07-23 00:00:00.000
600         2006-05-23 00:00:00.000

/***************************************************************************************
**										      **
** 		END OF CHAPTER 3. Practical 2			        	      **
**										      **
****************************************************************************************/






























/***************************************************************************************
**										      **
** 		CHAPTER 3. Basic Data Manipulation Practical 3.		  	      **
**		Nulls, Nullability, and 3 way logic				      **
** 										      **
****************************************************************************************/

Select * From salesperson

--p1) Produce a list of people who have a real (non null) post code value
--    (4 row result set).

SELECT	emp_no, post_code
FROM 	salesperson
WHERE	??????????



emp_no      post_code  
----------- ---------- 
10          RT8 8LP   
20          RF3 9UD   
30          W45 TY3   
50          CR1 2GH   

--p2) Now we want a similar result set, but ALL the salespeople must appear.
--    Display emp number and post code of the sales people, but if the post code is 'unknown'
--    then the  string 'Post Code Unknown' should be displayed instead.
--    Make use of the function 'COALESCE', ensuring the 2nd column (calculated) has a col heading.
--    This answer set should display only 2 COLUMNS (every row), as that is all the question asks for!




emp_no      Postcode          
----------- ----------------- 
10          RT8 8LP          
20          RF3 9UD          
30          W45 TY3          
40          Post Code Unknown
50          CR1 2GH          
60          Post Code Unknown

--p3) Run only the SELECT/FROM of this precoded query and then estimate how many rows will be 
--    returned if you then run it with the WHERE clause added.
--    Now uncomment the 'WHERE' and run all 3 lines. Did you estimate correctly?

SELECT	emp_no, post_code
FROM 	salesperson
--WHERE  post_code <> 'RT8 8LP'





--    Can you see how you could get 5 rows to appear?
SELECT	emp_no, post_code
FROM 	salesperson
--WHERE  post_code <> 'RT8 8LP' OR ????????????????

emp_no      post_code  
----------- ---------- 
20          RF3 9UD   
30          W45 TY3   
40          NULL
50          CR1 2GH   
60          NULL










-- IF YOU HAVE TIME 


-- Consider these 2 code samples, feel free to run them

CREATE VIEW PostCodeList
AS
SELECT emp_no, COALESCE(post_code, 'Post Code Unknown') AS Postcode
FROM   salesperson

The command(s) completed successfully.

SELECT *
FROM PostCodeList          -- treating the view as if it was a table

emp_no      Postcode          
----------- ----------------- 
10          RT8 8LP          
20          RF3 9UD          
30          W45 TY3          
40          Post Code Unknown
50          CR1 2GH          
60          Post Code Unknown

-- Can you see now how Coalesce can become useful? 
-- We do a chapter on Views later, but this should whet your appetite.


/***************************************************************************************
**										      **
** 		END OF CHAPTER 3. Practical 3.			        	      **
**										      **
****************************************************************************************/

