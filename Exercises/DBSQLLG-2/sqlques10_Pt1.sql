/***************************************************************************************
**										      **
** 		CHAPTER 10. Practical 1.				  	      **
**		Outer Joins revisited, Self Joins, Unions			      **
**										      **
****************************************************************************************/

Select * From dept
Select * From salesperson
Select * From sale
Select * From contact
Select * From company


--p1) We want to produce one list comprising three things all in one list
--	a) Manager names
--      b) Sales people's firstname and surname concatenated 
--      c) All Contacts names but only include those we have not sold to
--    The 1st column should be headed 'Name'
--    A second column called 'Type' should contain an 'm' or 'p' or 'c' as appropriate
--    to indicate manager/person/contact respectively
--    Sort the result set based on the second (type) column

????
UNION
????
UNION
????
Order by ????

-- produces
Name                            Type 
------------------------------- ---- 
Marvellous Marvin               c
Naughty Nick                    c
Ricky Rambo                     c
Uppy Umbrella                   c
Adam Apricot                    m
Barbara Banana                  m
Diver Dan                       m
Paul Peach                      m
Alan Brick                      p
Billy Custard                   p
Chris Digger                    p
Dick Ernst                      p
Ernest Flipper                  p
Fred Goalie                     p

(14 row(s) affected)


--p2) SELF JOINS
--    We want for some reason to know companies that are in the same county.
--    The following code solves the problem?, simply displaying them vertically ordered

SELECT county, name
FROM company
ORDER BY county

-- produces

county          name                 
--------------- -------------------- 
Devon           Kipper Kickers Inc
London          Judo Jeans PLC
London          Happy Heaters PLC
London          Icicle Igloos Inc

-- As there will be 'many' companies and there are relatively few counties
-- companies just can't help being in the same county as an other company(s)
-- But what if it was post_code matches we were looking for
-- A post code is one side of a street so with 10000 customers, there might be
-- 9950+ post_codes
-- The companies that share a post_code would be next to each other but hidden 
-- in the midst of a large list

-- This is where a self join might be helpful

SELECT C1.county, C1.name, C2.name
FROM   company C1 JOIN company C2
	ON C1.county = C2.county            -- don't join on company_no (anything but)

-- Can you picture what this would produce?

-- Try it





county          name                 name                 
--------------- -------------------- -------------------- 
London          Happy Heaters PLC    Happy Heaters PLC
London          Icicle Igloos Inc    Happy Heaters PLC
London          Judo Jeans PLC       Happy Heaters PLC
London          Happy Heaters PLC    Icicle Igloos Inc
London          Icicle Igloos Inc    Icicle Igloos Inc
London          Judo Jeans PLC       Icicle Igloos Inc
London          Happy Heaters PLC    Judo Jeans PLC
London          Icicle Igloos Inc    Judo Jeans PLC
London          Judo Jeans PLC       Judo Jeans PLC
Devon           Kipper Kickers Inc   Kipper Kickers Inc

-- each row joins to itself 'Happy' 'Happy' & 'Kipper' 'Kipper'
-- plus 'Happy' joins to 'Icicle' and 'Icicle' joins to 'Happy'

-- we can get rid of the 'Happy' & 'Happy' lines
-- by introducing a 'Where' clause
SELECT C1.county, C1.name, C2.name
FROM company C1 JOIN company C2
	ON C1.county = C2.county 
WHERE C1.company_no <> C2.company_no           -- notice the <>

-- produces

county          name                 name                 
--------------- -------------------- -------------------- 
London          Icicle Igloos Inc    Happy Heaters PLC
London          Judo Jeans PLC       Happy Heaters PLC
London          Happy Heaters PLC    Icicle Igloos Inc
London          Judo Jeans PLC       Icicle Igloos Inc
London          Happy Heaters PLC    Judo Jeans PLC
London          Icicle Igloos Inc    Judo Jeans PLC

-- now, can we make either the 'Happy' 'Icicle' row appear or 'Icicle' 'Happy' but not both?
-- yes, easy, change the '<>' to a '<' or a '>' (does'nt matter which'
-- so
SELECT C1.county, C1.name, C2.name
FROM company C1 JOIN company C2
	ON c1.county = c2.county 
WHERE C1.company_no < C2.company_no

-- produces

county          name                 name                 
--------------- -------------------- -------------------- 
London          Happy Heaters PLC    Icicle Igloos Inc
London          Happy Heaters PLC    Judo Jeans PLC
London          Icicle Igloos Inc    Judo Jeans PLC

-- but that is as good as it gets!

-- you could join company c1 to company c2 to company c3
-- and it would be possible to display all 3 companies 'Happy' 'Icicle' & 'Judo'
-- on the same line

-- but, if there were 4 in the same county it would then take 4 rows to tell you
-- if there were 5 in the same county it would give you 20 different combinations of 3!
-- 8 in the same county would give you 6720 combinations if displayed 3-up

-- surely we can do better, rest assured we can
-- an alternative to self joins is to list them vertically in sequence but only include
-- the rows that have a county that occurs more than once in the whole population

-- write a query that instead of listing these 4 rows
county          name                 
--------------- -------------------- 
Devon           Kipper Kickers Inc
London          Judo Jeans PLC
London          Happy Heaters PLC
London          Icicle Igloos Inc

-- just lists these 3 rows (leaving out the Devon row as Devon only appears once)
county          name                 
--------------- -------------------- 
London          Judo Jeans PLC
London          Happy Heaters PLC
London          Icicle Igloos Inc
 

-- Here is a starter for you

SELECT *
FROM company
WHERE county ?
COUNT(*) > 1



-- produces 

company_no  name                 tel             county          post_code  
----------- -------------------- --------------- --------------- ---------- 
1000        Happy Heaters PLC    (01306)345672   London          SE3 89L   
2000        Icicle Igloos Inc    0207-987-1265   London          N1 4LH    
3000        Judo Jeans PLC       0207-478-2990   London          N9 2FG    




















-- easy when you see it but few delegates crack that one
-- the principle is:
--    outer query is a list not a summary, so no Group By can't use Having
--    but selects rows based on criteria that searches into another result set
--    that is Grouped, does Count's and can use Having

-- Of course now you are an expert on inline views you could put your subquery 
-- in the from and join to it!
-- So it becomes not join 'me' to 'me' but join 'me' to a 'summarised/filtered version of me'!  
SELECT *
FROM company C JOIN (
		    SELECT county
		    FROM company
		    GROUP BY county
		    HAVING COUNT(*) > 1
		    ) GROUPED
ON C.county = GROUPED.county





-- Perhaps we have done enough SQL for one course now

/***************************************************************************************
**										      **
** 		END OF CHAPTER 10. Practical 1				       	      **
**										      **
****************************************************************************************/
