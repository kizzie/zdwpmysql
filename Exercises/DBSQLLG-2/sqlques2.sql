/***************************************************************************************
**										      			     **
** 		CHAPTER 2. Introduction to Relational Databases.        	           **
**										      			     **
** Familiarisation with the SQL Server interactive interface _______                  **
** and getting to know the data that you will work with on this course.		     **
**										      			     **
****************************************************************************************/

/*
	Please follow the instructions below, reading CAREFULLY at all times as the questions 
	have been thoughtfully worded.
*/

-- Part 1.

/* Shortly you will execute 5 sample 'SELECT' statements that retrieve all the data in all 
   the tables that you will use on this course. 
   
   These 5 'SELECT' statements will be REPEATED (precoded) at the top of many Chapters 
   Practicals in this file so it will be extremely easy to view a table's data at any time.
   
   The 5 tables were 'Created' as part of the course setup using some CREATE TABLE syntax 
   that you will learn fully later in the course but which looks something like this:
	
	CREATE TABLE dept (
		dept_no	INTEGER,
		dept_name   CHAR(20),
		etc, etc
			   )

   The 5 tables were then populated using INSERT INTO 'tablename' syntax which again you will
   learn fully this afternoon but which looks something like this:
	
	INSERT INTO dept (dept_no, dept_name,  etc, etc) -- the column names
	VALUES 		(1,'Animal Products', etc, etc)      -- the column values
   

   
*/


/* Scroll down now, please, so that this line is at the top of your display.
   You now need to perform some important steps which will assist you for the remainder of 
   the course. 
   This is a VERY important part of this course.

   For 2 days you are going to LISTEN (interact hopefully), TYPE CODE, HIGHLIGHT CODE, 
   EXECUTE CODE, decide from the displayed 'RESULTS' if your code was correct/incorrect, 
   TOGGLE back from the 'RESULTS' to your 'CODE' and either CORRECT it or go on to the 
   NEXT QUESTION, and hit FILE/SAVE quite often as well to ensure you don't lose anything!
   
   These tasks can be done TOTALLY via the 'keyboard' (the EASY way),
   or by a combination of keyboard(typing the code) and the 'mouse' (the HARD way).
   This is your chance to try out both and then decide if you going to be a 'mouse user' or 
   not on this course. 'Mouse' users are typically slower and more likely to run the wrong code.

   STEP 1. Highlight the following 'batch' of PRINT/SELECT statements. Here is how to do it.
   PRETEND that you have typed in the following 10 lines of PRINT/SELECT
   statements by placing your cursor IMMEDIATELY AFTER the 'y' of 'company' on the last of the
   10 lines and then whilst HOLDING DOWN the SHIFT key hit the UPARROW key until the whole of 
   the 10 lines are highlighted (please try it), once you have done this hit F5 to execute them.
*/

SELECT * FROM dept;
SELECT * FROM salesperson;
SELECT * FROM sale;
SELECT * FROM contact;
SELECT * FROM company ;                 

/*
   STEP 2. Having executed the code by hitting F5, recognize that the alternative method is to
    pick up the mouse and choose a menu item or toolbar icon representing 'EXECUTE QUERY'
-- Try both methods if you like and compare them.
   STEP 3. After executing the code you will get a split screen display.
-- This .SQL file will be in the top 'pane', the results of your query in the bottom 'pane'.
-- You can toggle the display of the results 'pane' on or off by using Ctrl-R or by  
-- clicking the rightmost icon in the toolbar.
-- Please try that now. 
*/


-- **NOTE** PRINT is not an SQL statement but is a SQL SERVER Transact-SQL language verb 
-- that simply sends text back to the client application message 
-- handler to make our output more readable during this familiarisation stage.
-- The SELECT statements are the real SQL! PRINT will not be mentioned again.

-- NOW FOR SOME REAL WORK
-- Part 2. 
--   By toggling the visibility of the "Results" pane and by scrolling the "Results" pane thru 
--   the 5 different tables type the answers to the following 10 questions.

--   These questions may seem very simple for a 'technical' IT training course, but in 
--   addition to getting the correct answer, analyse 'how you did it' as most if not all 
--   these questions will be answered by you with code, and the code will (eventually) bear a
--   striking similarity to the way you manually do the same thing.
--   You SHOULD NOT be WRITING ANY SQL YET!!

--1) Write down how many 'departments' will exist AFTER you remove departments managed by anyone 
--   with an 'X' in their name.

-- Answer - 

--2) How many departments have salespeople in them?

-- Answer - 

--3) Who (salesperson) has the biggest sales target?

-- Answer - 

--4) How many salespeople have sold?

-- Answer -

--5) To which contact (their name) was the sale on Nov 15th made?

-- Answer - 

--6) What was the post code of the company that the sale on Jan 23rd was made to?

-- Answer - 

--7) How many contacts have been sold to/not sold to?

-- Answer - 

--8) Choose the biggest sale and find the name of the manager of the dept that the sale 
--   'belongs to'.

-- Answer - 

--9) Which column is in three tables?

-- Answer - 

--10) Can you see which depts have no sales? If this is quite hard manually then it will
--    probably be 'hard' (but do-able) in code.

-- Answer -
/*
   To summarise this 'schema': depts employ salespeople who make sales to contacts who 
   work for companies. All code samples in slides and practicals will be based on 
   this data, although you will make small changes to the supplied data via the odd 
   Insert/Delete/Update statement later.
*/


-- sp_help is a MS SQL Server stored procedure that takes as an argument an object name
-- i.e a table name.
-- Try running this code:

sp_help salesperson

-- What did it get you? This is totally non standard but useful during these 2 days in a 
-- SQL Server environment. A similar Oracle command in the interactive Oracle SQL*Plus
-- environment would be: 

DESC salesperson -- as in 'Desc'ribe salesperson 

-- There is no ANSI Standard SQL statement to 'describe' a table. 

/***************************************************************************************
**										      **
** 		END OF CHAPTER 2. Introduction to Relational Databases.        	      **
**										      **
****************************************************************************************/

