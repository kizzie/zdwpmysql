/***************************************************************************************
**										      **
** 		CHAPTER 7. Practical 1.					  	      **
**		Views.					 			      **
**										      **
****************************************************************************************/
sp_help company

--p1)  Create a view named London.
--     It is going to show rows and columns from the 'company' table.
--     It should show company number, name and county columns.
--     It should only include rows in the London area (County = 'London'). 
--     When used for data entry it should NOT ALLOW non-London companies to be added.
--     Be a developer and Create the view.

CREATE VIEW ????
AS
SELECT ???
FROM ???
WHERE ????
??? ??? ???


--p2)  Now be the 'user' and SELECT everything (all rows and all columns) 
--     FROM the newly created view.  
--     (3 rows, with 3 columns each)




company_no  name                 county          
----------- -------------------- --------------- 
1000        Happy Heaters PLC    London
2000        Icicle Igloos PLC    London
3000        Judo Jeans PLC       London

-- p3)  Copy/paste the code from 1) and
--	     ALTER the view to ADD the 'post_code' column to the view so that it appears 
--      before 'county'.




--p4)  Now select from the VIEW again as you did in 2) above, but this time
--     only include 'name' and 'post_code' columns.
--     Include a WHERE clause that only SELECTs rows from the view if 
--     the post_code starts with an 'N'.
--     Order the rows by 'post_code'.


SELECT	?????
FROM 	London
WHERE	???? ?? ????



name                 post_code  
-------------------- ---------- 
Icicle Igloos PLC    N1 4LH    
Judo Jeans PLC       N9 2FG    


--p5)  Try to run this insert statement of a company based in 'Essex'. It shouldn�t be  allowed! 
--     If it is successful, you have forgotten 3 key words from your Create/Alter syntax.

INSERT INTO london
VALUES	(5000, 'ABC CO','RF45 7GH', 'ESSEX')

"The attempted insert or update failed because the target view either specifies 
WITH CHECK OPTION or spans a view that specifies WITH CHECK OPTION and one or more 
rows resulting from the operation did not qualify under the CHECK OPTION constraint.
The statement has been terminated. (SQL Server Message)"

-- The INSERT fails because the data violates the With Check Option condition.

-- If your insert did run successfully use this delete statement to remove the row.

DELETE FROM company WHERE company_no = 5000

/***************************************************************************************
**										      **
** 		END OF CHAPTER 7. Views					              **
**										      **
****************************************************************************************/

