/***************************************************************************************
**										      **
** 		CHAPTER 9. Practical 1.					  	      **
**		Standard Subqueries			 			      **
**										      **
****************************************************************************************/
SELECT * FROM dept
SELECT * FROM salesperson
SELECT * FROM sale
SELECT * FROM contact
SELECT * FROM company


--p1) Show the full name of the salesperson(s) (could be more than one) 
--    who has/have the largest sales target. 
--    (Answer - Billy Custard)




fname           lname              
--------------- ---------------  
Billy           Custard         

-- Question, was it safe to say '=' instead of 'IN'? Why?




--p2) We would like to write a query that displays the number '3'.
--    Why? Because that is the answer to "How many people have sold?".

--    Before starting, run the two queries supplied below, and work out the result (3) manually. 
--    We think there are only 2 ways of doing it. See if you can work out the 2 ways.  

--    Then try to write SQL that mimics exactly what you just did manually.
--    It can be done using a simple subquery. 
--    You may think of a way of doing it without a subquery.
--    Don't try any sort of JOIN!

SELECT emp_no AS 'emp nos of the people' FROM salesperson
SELECT emp_no AS 'emp nos who have sold' FROM sale ORDER BY emp_no


-- The answer is 3 so surely either coded solution query must start with:

SELECT 	COUNT(?)   -- you complete the rest.









/***************************************************************************************
**										      **
** 		END OF CHAPTER 9. Practical 1.			        	      **
**										      **
****************************************************************************************/































/***************************************************************************************
**										      **
** 		CHAPTER 9. Practical 2.					  	      **
**		Slightly harder subqueries and WHERE EXISTS			 			      **
**										      **
****************************************************************************************/

--p1) Display firstname, lastname, sales_target and the total of the sales(order_value) each 
--    salesperson has achieved.
--    We should ensure every salesperson is in the report not just the 3 who have sold.
--    There should be 1 row in the output for each salesperson (so 6 rows & 4 columns).
 
--    You know how to do this already, nothing new here, so to save time we are going
--    to build up the query together

--    The Select list is surely
SELECT	fname,	lname,	sales_target,	SUM(order_value) 'Sales achieved'


--    It clearly cannot run without a Group By, lets use Copy/Paste to complete the Group By
SELECT	fname,	lname,	sales_target,	SUM(order_value) 'Sales achieved'
GROUP BY fname,	lname,	sales_target




--    What's missing? A FROM clause. We need 2 tables joined and we want all salespeople,
--    not just the 3 who have sold so we must do an Outer join.
--    We�ve already covered Joins & Outer Joins, so complete the following code fragment by 
--    keeping/removing the word 'LEFT' or the word 'RIGHT' in the appropriate place and 
--    write the correct ON clause.

SELECT	fname,	lname,	sales_target,	SUM(order_value) 'Sales achieved'
FROM     salesperson SP LEFT/RIGHT  JOIN sale S        -- choose 'LEFT' or 'RIGHT'!
	                     ON SP.??? = S.??? 
GROUP BY fname,	lname,	sales_target 


--    Ensure you now have 6 rows 4 columns, note the NULL's in the last column
fname           lname           sales_target   Sales achieved 
--------------- --------------- -------------- -------------- 
Alan            Brick           9.00           8
Billy           Custard         14.00          NULL
Chris           Digger          7.00           NULL
Dick            Ernst           11.00          NULL
Ernest          Flipper         12.00          27
Fred            Goalie          13.00          30










-- p2) Tweak a copy of the code of 1) using the COALESCE function to ensure that the total sales of 
--     the 3 people who have not sold is 0 and not NULL. 

--     copy/paste your code here




fname           lname           sales_target   Sales achieved 
--------------- --------------- -------------- -------------- 
Alan            Brick           9.00           8
Billy           Custard         14.00          0
Chris           Digger          7.00           0
Dick            Ernst           11.00          0
Ernest          Flipper         12.00          27
Fred            Goalie          13.00          30










--p3) Let's now assess who would be interested in seeing this output.
--    The sales manager / sales director?
--    What would he/she be MOST interested in?
--    They would definitely be interested in how sales_target compares to sales achieved 
--    for each individual so that bonuses / warnings could be issued as appropriate,
--    but EVEN MORE interested in "does the total sales achieved by all 6 exceed the total
--    of the sales_targets of all 6?" - because that is probably what the sales manager's bonus 
--    is based on!!

--    You have been taught that WHERE EXISTS is used when you want to make an outer query run 
--    or not run based on whether a subquery produces any output.
--    We know that the sum of the targets of the 6 people is 66.
--    We also know that the sum of the order values of the 8 sales is currently 65.
--    So the following query would run. Check it.

SELECT	fname,	lname,	sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	                     ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(
	SELECT 'goobledegook'
	WHERE  66 > 65
	)
GROUP BY fname, lname, sales_target

--    Change the 65 to 67 (mimicking an extra sale of 2) and it won't run.

--    Obviously what we have to do is get rid of the hard coded numbers 65 & 66 and 
--    write SQL that calculates the correct ongoing values, 
--    so that the report is only produced if this statement is true
--    "the sum of the sales targets of all the sales people combined is greater than 
--    the total value of all the orders in sale",
--    
--    i.e. the salespeople as a WHOLE have not hit their COMBINED target.
--    This query is NOT based on any individual or dept but on the sum of everyone!

--    So, work on the above supplied query and ALL your work/typing MUST BE INSIDE the nested
--    inner query as the outer query is ALREADY PERFECT

SELECT	fname,	lname,	sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	                    ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(
	SELECT 'goobledegook' 
	-- put all your code in here !!!!!
	)
GROUP BY fname, lname, sales_target
















































-- This code following is INCORRECT as it double/triple... counts sales targets in the subquery
-- IF this is what you have coded then go back up and revisit the last code section
-- Ask your instructor for a clue if you need one

SELECT	fname, lname, sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	                     ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(SELECT	'Whatever' 
	 FROM 	salesperson SP1 JOIN sale S1          -- MUST NEVER JOIN 1 to MANY AND
                 ON SP1.emp_no = S1.emp_no 		      -- 
	 HAVING SUM(sales_target) > SUM(order_value)  -- USE A SUM FROM THE ONE!!!
	)

GROUP BY fname,	lname, sales_target


















--p4) Now that we have it working, let�s decide that if the report is produced then the person 
--    who sees it only needs to see the rows representing the salespeople who have 
--    NOT hit target (4 of the 6).

--    Copy the code of 3) down and make a small change to the solution to reduce the answer set 
--    to 4 rows as only 2 sales people have hit their target.

--    Note: you do not need to change any code you have written, just add some more!!
--    Remember there is nothing that is equal to null, so in a similar fashion nothing is ever 
--    'greater than' or 'less than' null!!  

SELECT	fname,	lname,	sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	                     ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(SELECT SUM(sales_target) 
	 FROM salesperson
	 HAVING SUM(sales_target) >
		(SELECT SUM(order_value)
		 FROM sale
		)
	)
GROUP BY SP.emp_no, fname, lname, sales_target
-- ADD SOME CODE HERE


fname           lname           sales_target   Sales achieved 
--------------- --------------- -------------- -------------- 
Alan            Brick           9.00           8
Billy           Custard         14.00          0
Chris           Digger          7.00           0
Dick            Ernst           11.00          0









-- p5) The following line from the result set catches your eye
fname           lname           sales_target   Sales achieved 
--------------- --------------- -------------- -------------- 
Alan            Brick           9.00           8


-- What business decision can you sensibly make given that you know
-- what the underlying SQL is doing?
-- Is it safe to assume Alan is just under target and will exceed it with the next sale?
 
-- What you must consider is this: If there are 2 salespeople both called Alan Brick,
-- will they get 
-- a) a row each in the output 
--    or
-- b) be put in the same bucket, giving us one row in the output representing 
--    all people called 'Alan Brick'?

-- Please decide now. Make a decision. Don't worry if it is wrong.
-- Only then scroll down a page to see the answer.
























































-- The answer is neither!!
-- If two salespeople called Alan Brick had the SAME sales_target then they would be treated as 
-- 1 person and there would be one 'Alan Brick' row showing the result of their combined performance
-- Remember the grouping is by fname, lname, & SALES_TARGET, not just by name(s).

-- The problem is that the query does not contain emp_no.
-- You might say it does, but it doesn�t. It's appearance in the ON clause to make 
-- the correct join is a complete irrelevance with regard to the GROUP BY. 

-- So making the following change to the GROUP BY of the query ensures that 
-- each 'Alan Brick' gets his own row, even if they have the same sales_target.
 
SELECT	fname,	lname,	sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	                     ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(SELECT SUM(sales_target) 
	 FROM salesperson
	 HAVING SUM(sales_target) >
		(SELECT SUM(order_value)
		 FROM sale
		)
	)
GROUP BY SP.emp_no, fname, lname, sales_target
         --------*                 -- new column added       
HAVING sales_target > COALESCE(SUM(order_value),0)


-- The problem then is that potentially in the future the output might look like this:

fname           lname           sales_target   Sales achieved 
--------------- --------------- -------------- -------------- 
Alan            Brick           9.00           6
Alan            Brick           9.00           2
Billy           Custard         14.00          0
Chris           Digger          7.00           0
Dick            Ernst           11.00          0

-- but with no guarantee that the two 'Alan Brick' rows are anywhere near each other or even
-- on the same page of the report.
-- Question: How would your 'users' read the above query? Would they realize that the 2 rows
-- represent 2 different people?

-- Surely if SP.emp_no is in the GROUP BY, it should be in the Select list,

-- i.e.
SELECT	SP.emp_no, fname, lname, sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
        --------*                         -- column added
FROM 	salesperson SP LEFT JOIN sale S
	                     ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(SELECT SUM(sales_target) 
	 FROM salesperson
	 HAVING SUM(sales_target) >
		(SELECT SUM(order_value)
		 FROM sale
		)
	)
GROUP BY SP.emp_no, fname, lname, sales_target
         --------*
HAVING sales_target > COALESCE(SUM(order_value),0)

-- So the output would now be (with one Alan Brick)

emp_no      fname           lname           sales_target   Sales achieved 
----------- --------------- --------------- -------------- -------------- 
10          Alan            Brick           9.00           8
20          Billy           Custard         14.00          0
30          Chris           Digger          7.00           0
40          Dick            Ernst           11.00          0

-- and, if there were 2 Alan Bricks, then the output would show clearly
-- that they are 2 different people.

emp_no      fname           lname           sales_target   Sales achieved 
----------- --------------- --------------- -------------- -------------- 
10          Alan            Brick           9.00           6
20          Billy           Custard         14.00          0
30          Chris           Digger          7.00           0
40          Dick            Ernst           11.00          0
90          Alan            Brick           9.00           2

-- THE MORAL IS: "WRITE THE CORRECT 'GROUP BY' FIRST & THEN COPY/PASTE INTO THE 'SELECT' LIST"
-- NOT THE REVERSE as you might have surmised from earlier study/lectures.
   ***************


-- END OF 'MANDATORY PART OF PRACTICAL'









-- If you have time


--p6) Can you achieve the same output WITHOUT using EXISTS? Yes, with a small code change.
--    Hint: make the Where clause look like:

WHERE (subquery returning one number) >  (subquery returning another number)  

-- Everyone would agree that: 
-- This code is
-- a) simpler to read
-- b) simpler to write
-- c) easier to maintain
-- d) only works because these subqueries each return a 1 row 1 column 'atomic' number.
-- We have NOT broken the rule of "cannot use an aggregate in a Where clause";
--    we used it in a subquery of the where clause, an important difference
-- In SQL, as in life, there are many ways of achieving the same end!

-- As a student your question might be "which will be faster?"
-- Answer: too many factors to determine easily, but if your DBMS has a 
-- 'show execution plan/costs' feature you could run the two together and see.

-- Last time we ran the code of p6) alongside the code of p5) (with EXISTS) the Execution plans 
-- in SQL SERVER 2000 were absolutely IDENTICAL and the costs of each were 50% of the batch! 

-- Later in the course we will see other examples of 'different' ways to achieve the same    end.










-- p7) For your consideration / information.
--     How do we find the names associated with the top 2 sales_target(s)?

SELECT fname, lname, sales_target
FROM   salesperson
ORDER BY sales_target DESC

fname           lname           sales_target   
--------------- --------------- -------------- 
Billy           Custard         14.00
Fred            Goalie          13.00
Ernest          Flipper         12.00
Dick            Ernst           11.00
Alan            Brick           9.00
Chris           Digger          7.00

-- That does it. Just read the top 2 rows from the output of 6 rows,
-- but somewhat inefficient if you had 600/6000/60000 rows.






-- Selecting the top few rows
-- ==========================

-- Very nice SQL Server option (since about 1997). 

SELECT TOP 2 	fname, lname, sales_target
FROM 		salesperson
ORDER BY	sales_target DESC

fname           lname           sales_target   
--------------- --------------- -------------- 
Billy           Custard         14.00
Fred            Goalie          13.00


-- But what if there was another person who had a target of 13, could I get them as well?
-- Yes, via 'with ties'.  
SELECT TOP 2 WITH TIES fname, lname, sales_target
FROM 		        salesperson
ORDER BY	        sales_target DESC

-- You can base your question on percentages as in:

SELECT TOP 35 PERCENT WITH TIES fname, lname, sales_target
FROM 		                salesperson
ORDER BY	                sales_target DESC

-- Oracle achieves similar functionality via 'inline views'.
SELECT  	ROWNUM AS Rank, fname, lname, sales_target
FROM 		(
		SELECT fname, lname, sales_target
		FROM salesperson
		ORDER BY sales_target DESC
		)                    -- first view of a subquery in a FROM clause! 
WHERE 		ROWNUM <= 2          -- 'ROWNUM' is a special pseudo column in Oracle 



/***************************************************************************************
**										      **
** 		END OF CHAPTER 9. Practical 2.			        	      **
**										      **
****************************************************************************************/































