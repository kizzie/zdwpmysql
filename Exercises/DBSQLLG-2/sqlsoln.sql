/***************************************************************************************
**										      			     **
** 		CHAPTER 2. Introduction to Relational Databases.        	           **
**										      			     **
** Familiarisation with the SQL Server interactive interface _______                  **
** and getting to know the data that you will work with on this course.		     **
**										      			     **
****************************************************************************************/

/*
	Please follow the instructions below, reading CAREFULLY at all times as the questions 
	have been thoughtfully worded.
*/

-- Part 1.

/* Shortly you will execute 5 sample 'SELECT' statements that retrieve all the data in all 
   the tables that you will use on this course. 
   
   These 5 'SELECT' statements will be REPEATED (precoded) at the top of many Chapters� 
   Practicals in this file so it will be extremely easy to view a table's data at any time.
   
   The 5 tables were 'CREATEd' as part of the course setup using some CREATE TABLE syntax 
   that you will learn fully later in the course but which looks something like this:
	
	CREATE TABLE dept (
		dept_no	    INTEGER,
		dept_name   CHAR(20),
		etc, etc
			   )

   The 5 tables were then populated using INSERT INTO 'tablename' syntax which again you will
   learn fully this afternoon but which looks something like this:
	
	INSERT INTO dept (dept_no, dept_name,  etc, etc) -- the column names
	VALUES 		(1,'Animal Products', etc, etc)      -- the column values
   

   
*/

/* Scroll down now, please, so that this line is at the top of your display.
   You now need to perform some important steps which will assist you for the remainder of 
   the course. 
   This is a VERY important part of this course.

   For 2 days you are going to LISTEN (interact hopefully), TYPE CODE, HIGHLIGHT CODE, 
   EXECUTE CODE, decide from the displayed 'RESULTS' if your code was correct/incorrect, 
   TOGGLE back from the 'RESULTS' to your 'CODE' and either CORRECT it or go on to the 
   NEXT QUESTION, and hit FILE/SAVE quite often as well to ensure you don't lose anything!
   
   These tasks can be done TOTALLY via the 'keyboard' (the EASY way),
   or by a combination of keyboard(typing the code) and the 'mouse' (the HARD way).
   This is your chance to try out both and then decide if you going to be a 'mouse user' or 
   not on this course. 'Mouse' users are typically slower and more likely to run the wrong code.

   STEP 1. Highlight the following 'batch' of PRINT/SELECT statements. Here is how to do it.
   PRETEND that you have typed in the following 10 lines of PRINT/SELECT
   statements by placing your cursor IMMEDIATELY AFTER the 'y' of 'company' on the last of the
   10 lines and then whilst HOLDING DOWN the SHIFT key hit the UPARROW key until the whole of 
   the 10 lines are highlighted (please try it), once you have done this hit F5 to execute them.
*/

PRINT 'Here are the "departments"'
SELECT * FROM dept
PRINT 'Here are the "salespeople" (they work in "departments")'
SELECT * FROM salesperson
PRINT 'Here are the "sales" made by the "salespeople" (to customer "contacts")'
SELECT * FROM sale
PRINT 'Here are the "contacts" (who work for "companies"), that the "sales" were made to'
SELECT * FROM contact
PRINT 'Here are those "companies" (that the "contacts" work for)'
SELECT * FROM company                  

/*
   STEP 2. Having executed the code by hitting F5, recognize that the alternative method is to
    pick up the mouse and choose a menu item or toolbar icon representing 'EXECUTE QUERY'
-- Try both methods if you like and compare them.
   STEP 3. After executing the code you will get a split screen display.
-- This .SQL file will be in the top 'pane', the results of your query in the bottom 'pane'.
-- You can toggle the display of the results 'pane' on or off by using Ctrl-R or by  
-- clicking the rightmost icon in the toolbar.
-- Please try that now. 
*/


-- **NOTE** PRINT is not an SQL statement but is a SQL SERVER Transact-SQL language verb 
-- that simply sends text back to the client application message 
-- handler to make our output more readable during this familiarisation stage.
-- The SELECT statements are the real SQL! PRINT will not be mentioned again.

-- NOW FOR SOME REAL WORK
-- Part 2. 
--   By toggling the visibility of the "Results" pane and by scrolling the "Results" pane thru 
--   the 5 different tables type the answers to the following 10 questions.

--   These questions may seem very simple for a 'technical' IT training course, but in 
--   addition to getting the correct answer, analyse 'how you did it' as most if not all 
--   these questions will be answered by you with code, and the code will (eventually) bear a
--   striking similarity to the way you manually do the same thing.
--   You SHOULD NOT be WRITING ANY SQL YET!!

--1) Write down how many 'departments' will exist AFTER you remove departments managed by anyone 
--   with an 'X' in their name.

-- Answer - 4

--2) How many departments have salespeople in them?

-- Answer - 3

--3) Who (salesperson) has the biggest sales target?

-- Answer - Billy Custard

--4) How many salespeople have sold?

-- Answer - 3

--5) To which contact (their name) was the sale on Nov 15th made?

-- Answer - 'Robber Red'

--6) What was the post code of the company that the sale on Jan 23rd was made to?

-- Answer - 'N1 4LH'

--7) How many contacts have been sold to/not sold to?

-- Answer - 6 & 3

--8) Choose the biggest sale and find the name of the manager of the dept that the sale 
--   'belongs to'.

-- Answer - Order '600' for '27' by emp_no '50' in dept '3' managed by 'Paul Peach'

--9) Which column is in three tables?

-- Answer - company_no

--10) Can you see which depts have no sales? If this is quite hard manually then it will
--    probably be 'hard' (but do-able) in code.

-- Answer - depts 2,4 & 5, because the sales are made by emps 10,50 & 60 who work in depts 1 & 3

/*
   To summarise this 'schema': depts employ salespeople who make sales to contacts who 
   work for companies. All code samples in slides and practicals will be based on 
   this data, although you will make small changes to the supplied data via the odd 
   INSERT/DELETE/UPDATE statement later.
*/


-- sp_help is a MS SQL Server stored procedure that takes as an argument an object name
-- i.e a table name.
-- Try running this code:

sp_help salesperson

-- What did it get you? This is totally non standard but useful during these 2 days in a 
-- SQL Server environment. A similar Oracle command in the interactive Oracle SQL*Plus
-- environment would be: 

DESC salesperson -- as in 'Desc'ribe salesperson 

-- There is no ANSI Standard SQL statement to 'describe' a table. 

/***************************************************************************************
**										      **
** 		END OF CHAPTER 2. Introduction to Relational Databases.        	      **
**										      **
****************************************************************************************/
































/***************************************************************************************
**										      **
** 		CHAPTER 3. Basic Data Manipulation Practical 1.		  	      **
**		Limiting columns, calculations, aliases, sorting and duplicates	      **
** 										      **
****************************************************************************************/
SELECT * FROM salesperson
SELECT * FROM sale

--p1) Look at your supplied 'schema'. 
--    Choose ONE of those 5 tables and write the FROM clause.
--    Focus your eyes on the column names of that table.
--    Now SELECT any THREE columns of your choice from that table.
--    Your code should resemble this structure:

--    SELECT columnA, columnB, columnC
--    FROM tableZ

--    You decide what tableZ, columnA, columnB, columnC are!!

SELECT  emp_no, fname, lname
FROM	salesperson

emp_no      fname           lname           
----------- --------------- --------------- 
10          Alan            Brick
20          Billy           Custard
30          Chris           Digger
40          Dick            Ernst
50          Ernest          Flipper
60          Fred            Goalie

 
--p2) We would like you to list all the salespeople, so write the FROM clause now.

--    Some INFORMATION for you - the sales_targets of the salespeople are YEARLY targets.  
--    We would like you to display (SELECT) 2 columns only.
--    Please display the SURNAMES and the QUARTERLY target of each salesperson.
--    One of the 2 columns you have just SELECTed is a Calculated Column.
--    This column should have a heading (ALIAS) of 'Quarterly Target'. 
--    We would also like the rows to be sorted by these quarterly targets, 
--    but with the largest value appearing first.

SELECT		lname, sales_target / 4 'Quarterly Target'
FROM		salesperson
ORDER BY 	'Quarterly Target' DESC

lname           Quarterly Target   
--------------- ------------------ 
Custard         3.500000
Goalie          3.250000
Flipper         3.000000
Ernst           2.750000
Brick           2.250000
Digger          1.750000


--p3) Display 3 columns, each salesperson's employee number, dept number and initials.
--    The initials are not in the table but can be retrieved from the firstname and lastname.
--    They should be displayed as one value separated by a fullstop, eg. Alan Brick should be �A.B�.
--    The answer set should have a column heading saying 'Initials'.
--    REMINDER, the function 'SUBSTRING' has EXACTLY 3 MANDATORY parameters.

SELECT 	emp_no,
	dept_no,
	SUBSTRING(fname,1,1) + '.' + SUBSTRING(lname,1,1) 'Initials'
FROM 	salesperson

emp_no      dept_no     Initials 
----------- ----------- -------- 
10          1           A.B
20          2           B.C
30          2           C.D
40          3           D.E
50          3           E.F
60          3           F.G


--p4) List the employee numbers of those salespeople who have sold. 
--    Remove duplicates!! - (3 row result set)

SELECT	DISTINCT emp_no
FROM 	sale

emp_no      
----------- 
10
50
60

-- IF YOU HAVE TIME

--p5) From the salesperson table list the dept_no/county combinations. Again, 
--    no duplicate entries please. - (4 rows)

SELECT	DISTINCT dept_no, county
FROM 	salesperson

dept_no     county          
----------- --------------- 
1           Surrey
2           Hampshire
3           London
3           Surrey

--p6) List (sorted) just the month numbers of the 8 sales. 

SELECT DATEPART(month, order_date) as month
FROM sale
ORDER BY month 

month       
----------- 
1
5
5
6
7
7
8
11

/***************************************************************************************
**										      **
** 		END OF CHAPTER 3. Practical 1			        	      **
**										      **
****************************************************************************************/

 






































/***************************************************************************************
**										      **
** 		CHAPTER 3. Basic Data Manipulation Practical 2.		  	      **
**		Using a 'WHERE' clause to restrict rows				      **
**                                                                                    **
****************************************************************************************/

-- Just in case you need them:
SELECT * FROM dept
SELECT * FROM sale
SELECT * FROM contact

-- ******** NOTE
-- If you are not instructed which columns to 'SELECT' then you choose, or put '*'.
-- This practical is testing your ability to write a 'WHERE' clause.
-- ******** NOTE


--p1) Display only those sales whose order value is in the range 10 to 30 inclusive 
--    (2 row result set). Note which employee(s) made these 2 sales.

SELECT 	*
FROM 	sale
WHERE	order_value between 10 and 30


order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
300         60          2000        OO           12          2006-07-14 00:00:00.000                                ScanPRO 4800 Scanner
600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System

--p2) Copy/Paste Part 1 and amend the WHERE clause so that we further restrict these sales 
--    to just those made by either employee 10 or employee 50 (1 row result set).

--    Write the code now here.

SELECT 	*
FROM 	sale
WHERE	order_value BETWEEN 10 and 30
AND	emp_no IN (10, 50)

order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System


-- Did you just use the word 'OR' in the last bit of code?
-- If you did, and used no parentheses i.e. '(' and  ')', then you asked the the wrong question
-- although you might appear to have the correct answer!!
-- Hopefully you used the 'IN' word which avoided using 'OR'. If you didn�t, ensure you now do.




















-- This SQL is INCORRECT, as it would show all sales by emp_no 50 regardless of value:

SELECT 	* 
FROM	sale
WHERE	order_value BETWEEN 10 AND 30
AND	emp_no = 10 OR emp_no = 50



--p3) Produce firstly a list of departments whose name contains  'SYS'. 
--    Get this working first!! (depts 2 & 4 should appear).

--    Now amend the query to ALSO include any depts whose sales_target is less than 10.
--    Get that working now (depts 1, 2 & 4 should appear).

--    But we DO NOT want to see under ANY circumstances 
--    a row in the answer set that has 'Barbara Banana' as manager.

--    Amend the query further to reflect this requirement (depts 1 & 4 ONLY should now appear).
--    If you have the wrong answer, OR if you have used no parentheses, then revisit your code!!

SELECT 	*
FROM 	dept
WHERE 	(dept_name LIKE '%SYS%' OR sales_target < 10)
AND 	manager <> 'Barbara Banana'

dept_no     dept_name            manager              sales_target   
----------- -------------------- -------------------- -------------- 
1           Animal Products      Adam Apricot         5.00
4           Desktop Systems      Diver Dan            5.00








-- ONLY IF YOU HAVE TIME

--p4) Display contact names and tel numbers in Inner London.
--    Inner London is '0207'.
--    Note: tel numbers will never be stored as numeric columns because you don't 
--    ever want to do arithmetic on them!
--    (Correct answer is 3 rows, not 2 and not 4)
--    You need to work around some very 'iffy'/'dodgy'/'bad' data.

SELECT	name, tel
FROM 	contact
WHERE	SUBSTRING(tel,1,5) LIKE '%0207%' 

name                 tel                       
-------------------- ------------------------- 
Munching Mike        (0207)223-9887           
Ollie Octopus        0207-341-566670 ext 10   
Ricky Rambo          0207-988-0777       

-- If you are not happy with your answer, then don't worry, we will REVIEW after practical!

--p5) List sales made in May and July (any year).
--    Use the MS SQL Server date function called 'DATEPART', Shift-F1 for TRANSACT-SQL HELP. 
--    (4 rows)

SELECT 	order_no, order_date
FROM	sale
WHERE	DATEPART(mm,order_date) IN (5,7)

order_no    order_date                                             
----------- ------------------------------------------------------ 
200         2006-05-01 00:00:00.000
300         2006-07-14 00:00:00.000
500         2006-07-23 00:00:00.000
600         2006-05-23 00:00:00.000

/***************************************************************************************
**										      **
** 		END OF CHAPTER 3. Practical 2			        	      **
**										      **
****************************************************************************************/






























/***************************************************************************************
**										      **
** 		CHAPTER 3. Basic Data Manipulation Practical 3.		  	      **
**		Nulls, Nullability, and 3 way logic				      **
** 										      **
****************************************************************************************/

SELECT * FROM salesperson

--p1) Produce a list of people who have a real (non null) post code value
--    (4 row result set).


SELECT	emp_no, post_code
FROM 	salesperson
WHERE 	post_code IS NOT NULL


emp_no      post_code  
----------- ---------- 
10          RT8 8LP   
20          RF3 9UD   
30          W45 TY3   
50          CR1 2GH   

--p2) Now we want a similar result set, but ALL the salespeople must appear.
--    Display emp number and post code of the sales people, but if the post code is 'unknown'
--    then the  string 'Post Code Unknown' should be displayed instead.
--    Make use of the function 'COALESCE', ensuring the 2nd column (calculated) has a col heading.
--    This answer set should display only 2 COLUMNS (every row), as that is all the question asks for!

SELECT emp_no, COALESCE(post_code, 'Post Code Unknown') AS Postcode
FROM   salesperson

emp_no      Postcode          
----------- ----------------- 
10          RT8 8LP          
20          RF3 9UD          
30          W45 TY3          
40          Post Code Unknown
50          CR1 2GH          
60          Post Code Unknown

--p3) Run only the SELECT/FROM of this precoded query and then estimate how many rows will be 
--    returned if you then run it with the WHERE clause added.
--    Now uncomment the 'WHERE' and run all 3 lines. Did you estimate correctly?

SELECT	emp_no, post_code
FROM 	salesperson
--WHERE  post_code <> 'RT8 8LP'




--    Can you see how you could get 5 rows to appear?
SELECT	emp_no, post_code
FROM 	salesperson
--WHERE  post_code <> 'RT8 8LP' OR ????????????????

emp_no      post_code  
----------- ---------- 
20          RF3 9UD   
30          W45 TY3   
40          NULL
50          CR1 2GH   
60          NULL








-- IF YOU HAVE TIME

-- Consider these 2 code samples, feel free to run them

CREATE VIEW PostCodeList
AS
SELECT emp_no, COALESCE(post_code, 'Post Code Unknown') AS Postcode
FROM   salesperson

The command(s) completed successfully.

SELECT *
FROM PostCodeList          -- treating the view as if it was a table

emp_no      Postcode          
----------- ----------------- 
10          RT8 8LP          
20          RF3 9UD          
30          W45 TY3          
40          Post Code Unknown
50          CR1 2GH          
60          Post Code Unknown

-- Can you see now how Coalesce can become useful? 
-- We do a chapter on VIEWs later, but this should whet your appetite.


/***************************************************************************************
**										      **
** 		END OF CHAPTER 3. Practical 3.			        	      **
**										      **
****************************************************************************************/





















/***************************************************************************************
**										      **
** 		CHAPTER 4. Practical 1.					  	      **
**		INSERTing & Deleting Data					      **
** 										      **
****************************************************************************************/


--p1)  You are going to INSERT 1 row into the contact table.
--     You are not going to bother to type in the full comma separated list of column names, 
--     simply the right number of column values(6) in the CORRECT sequence.
--     Check the sequence against your schema.
--     The contact should be in company 4000.
--     His/her contact code is to be 'MM'.
--     You invent a name and job-title.
--     Ensure the last 2 columns of the table, 'tel' & 'notes', are NULL.

INSERT INTO contact
VALUES (4000, 'MM', 'Marvellous Marvin', 'Magician', NULL, NULL)


--p2)  After successfully adding yourself to the table check you are there. (run this code)

SELECT  *
FROM	contact

company_no  contact_code name                 job_title                      tel                       notes                                                        
----------- ------------ -------------------- ------------------------------ ------------------------- ------------------------------------------------------------ 
1000        MM           Munching Mike        Accounts Officer               (0207)223-9887            We first visited her in January 2006
2000        NN           Naughty Nick         Bought Ledger Manager          01546-456566 Ext 22       Works only on Monday and Wednesdays
2000        OO           Ollie Octopus        Chief Executive Officer        0207-341-566670 ext 10    
3000        PP           Purposeful Peter     Development Director           0131 324545 ext 213       Insists on personally signing all orders
3000        QQ           Quentin Quail        Electrical Manager             01456 802071 ext 44       Has been in his job a long time
3000        RR           Robber Red           Federal Reporting Officer      0356-345345               Has a preference for Apple Macs
4000        MM           'Your Name'          'Your Title'                   NULL                      NULL
4000        RR           Ricky Rambo          Gourmet Foods Purchaser        0207-988-0777             
4000        TT           Terrible Tim         Head of Inter Office Systems   05673-476878 ext 221      Is listed in Whos Who 2005 onwards
4000        UU           Uppy Umbrella        Accounts Officer               0823-598494 ext 1         Is upset because he is not chief executive!

--p3)  DELETE any department whose manager's name contains the character 'x'.
--     Check who should be DELETEd before running the DELETE, by using your proposed
--     WHERE clause in a SELECT first.

SELECT 	manager
FROM 	dept
WHERE 	manager LIKE '%X%'

manager              
-------------------- 
Xavier Xylophone    

DELETE  FROM dept
WHERE 	manager LIKE '%X%'

(1 row(s) affected)

SELECT * 
FROM dept

dept_no     dept_name            manager              sales_target   
----------- -------------------- -------------------- -------------- 
1           Animal Products      Adam Apricot         5.00
2           Business Systems     Barbara Banana       15.00
3           Credit Control       Paul Peach           25.00
4           Desktop Systems      Diver Dan            5.00


/***************************************************************************************
**										      **
** 		END OF CHAPTER 4. Practical 1.			        	      **
**										      **
****************************************************************************************/















































/***************************************************************************************
**										      **
** 		CHAPTER 4. Practical 2.					  	      **
**		Updating Data					                      **
** 										      **
****************************************************************************************/


--p1)  Departments have sales_targets, so do salespeople.
--     We want to change the sales_targets of each DEPT.

--     Each dept's target should be INCREASED by 5 'units'(you don't care what the 'unit' is).
--     Insert your code BETWEEN the 2 SELECT statements that follow 

--     Run the 3 statements TOGETHER, ONCE and ONCE ONLY. 
--     This will enable you (one time only) to see the 'before' and 'after' version of the 
--     row(s) you change.

SELECT * FROM dept

UPDATE 	dept 
SET 	sales_target = sales_target + 5  

SELECT * FROM dept

-- before image
dept_no     dept_name            manager              sales_target   
----------- -------------------- -------------------- -------------- 
1           Animal Products      Adam Apricot         5.00
2           Business Systems     Barbara Banana       15.00
3           Credit Control       Paul Peach           25.00
4           Desktop Systems      Diver Dan            5.00
(4 row(s) affected) 

-- the UPDATE
(4 row(s) affected) 

-- after image
dept_no     dept_name            manager              sales_target   
----------- -------------------- -------------------- -------------- 
1           Animal Products      Adam Apricot         10.00
2           Business Systems     Barbara Banana       20.00
3           Credit Control       Paul Peach           30.00
4           Desktop Systems      Diver Dan            10.00
(4 row(s) affected) 

--p2)  Change the contact Ricky Rambo's job title to 'VP of Development'
--     and SIMULTANEOUSLY change his/her telephone number to '01242-112233 Ext 444'.
--     Do this in ONE statement. INSERT your code between the 2 precoded 'SELECTs' and 
--     then run all 3 statements again.

SELECT * FROM contact

UPDATE 	contact
SET 	job_title =  'VP of Development', tel = '01242-112233 Ext 444'
WHERE 	name = 'Ricky Rambo'

SELECT * FROM contact

-- before image
company_no  contact_code name                 job_title                      tel                       notes                                                        
----------- ------------ -------------------- ------------------------------ ------------------------- ------------------------------------------------------------ 
1000        MM           Munching Mike        Accounts Officer               (0207)223-9887            We first visited her in January 2006
2000        NN           Naughty Nick         Bought Ledger Manager          01546-456566 Ext 22       Works only on Monday and Wednesdays
2000        OO           Ollie Octopus        Chief Executive Officer        0207-341-566670 ext 10    
3000        PP           Purposeful Peter     Development Director           0131 324545 ext 213       Insists on personally signing all orders
3000        QQ           Quentin Quail        Electrical Manager             01456 802071 ext 44       Has been in his job a long time
3000        RR           Robber Red           Federal Reporting Officer      0356-345345               Has a preference for Apple Macs
4000        MM           Marvellous Marvin    Magician                       NULL                      NULL
4000        RR           Ricky Rambo          Gourmet Foods Purchaser        0207-988-0777             
4000        TT           Terrible Tim         Head of Inter Office Systems   05673-476878 ext 221      Is listed in Whos Who 2005 onwards
4000        UU           Uppy Umbrella        Accounts Officer               0823-598494 ext 1         Is upset because he is not chief executive!

(10 row(s) affected)

-- the UPDATE
(1 row(s) affected)

-- after image
company_no  contact_code name                 job_title                      tel                       notes                                                        
----------- ------------ -------------------- ------------------------------ ------------------------- ------------------------------------------------------------ 
1000        MM           Munching Mike        Accounts Officer               (0207)223-9887            We first visited her in January 2006
2000        NN           Naughty Nick         Bought Ledger Manager          01546-456566 Ext 22       Works only on Monday and Wednesdays
2000        OO           Ollie Octopus        Chief Executive Officer        0207-341-566670 ext 10    
3000        PP           Purposeful Peter     Development Director           0131 324545 ext 213       Insists on personally signing all orders
3000        QQ           Quentin Quail        Electrical Manager             01456 802071 ext 44       Has been in his job a long time
3000        RR           Robber Red           Federal Reporting Officer      0356-345345               Has a preference for Apple Macs
4000        MM           Marvellous Marvin    Magician                       NULL                      NULL
4000        RR           Ricky Rambo          VP of Development              01242-112233 Ext 444      
4000        TT           Terrible Tim         Head of Inter Office Systems   05673-476878 ext 221      Is listed in Whos Who 2005 onwards
4000        UU           Uppy Umbrella        Accounts Officer               0823-598494 ext 1         Is upset because he is not chief executive!

(10 row(s) affected)

--p3) IF YOU HAVE TIME

-- Run this
SELECT * FROM company

-- old image
company_no  name                 tel             county          post_code  
----------- -------------------- --------------- --------------- ---------- 
1000        Happy Heaters PLC    (01306)345672   London          SE3 89L   
2000        Icicle Igloos Inc    0207-987-1265   London          N1 4LH    
3000        Judo Jeans PLC       0207-478-2990   London          N9 2FG    
4000        Kipper Kickers Inc   01254-987766    Devon           PL4 9RT   

-- 'Icicle Igloos Inc' has just floated on the Stock Exchange.
-- Change it's company name to 'Icicle Igloos PLC'.

UPDATE company
SET name = 'Icicle Igloos PLC'
WHERE company_no = 2000   -- or 'WHERE name = 'Icicle Igloos Inc'

(1 row(s) affected)

-- new image
company_no  name                 tel             county          post_code  
----------- -------------------- --------------- --------------- ---------- 
1000        Happy Heaters PLC    (01306)345672   London          SE3 89L   
2000        Icicle Igloos PLC    0207-987-1265   London          N1 4LH    
3000        Judo Jeans PLC       0207-478-2990   London          N9 2FG    
4000        Kipper Kickers Inc   01254-987766    Devon           PL4 9RT   



/***************************************************************************************
**										      **
** 		END OF CHAPTER 4. Practical 2			         	      **
**										      **
****************************************************************************************/
































/***************************************************************************************
**										      **
** 		CHAPTER 5. Practical 1.					  	      **
**	JOINing related tables.	INNER, Cross, Composite JOINs, Ambiguity, Aliases     **
** 										      **
****************************************************************************************/

SELECT * FROM dept
SELECT * FROM salesperson
SELECT * FROM sale
SELECT * FROM company
SELECT * FROM contact

--p1) Display order number, order value and the NAME of the company each sale was made to.
--    If you are not sure what to JOIN on then run the 'sale' and 'company' SELECT statements 
--    above and choose a sale at random and decide the name of the company it was made to and 
--    write the code to mimic what you just did.

--    Use TABLE ALIASES, please, a good habit to get into early on. 

--    Sort the answer set by company name. (8 rows)

--    Notice all 4 companies have been sold to (relevant later in the course).


SELECT 	order_no, order_value, name
FROM 	sale S INNER JOIN company C
	ON S.company_no = C.company_no
ORDER BY name

order_no    order_value name                 
----------- ----------- -------------------- 
100         7           Happy Heaters PLC
400         5           Happy Heaters PLC
300         12          Icicle Igloos PLC
700         3           Icicle Igloos PLC
800         3           Judo Jeans PLC
600         27          Judo Jeans PLC
200         6           Judo Jeans PLC
500         2           Kipper Kickers Inc


--p2) Totally new query. 
--    Show all sales.
--    Display from each sale the order_value and description columns.

--    Then, additionally, on the left hand side of the display show the full name and dept number
--    of the person who made the sale.  Use TABLE ALIASES please.

--    Notice the 8 sales have been made by 2 depts (3 people) only, relevant later.
--    (8 row result set)

SELECT 	fname, lname, dept_no, order_value, description
FROM	sale S INNER JOIN salesperson SP
	ON S.emp_no = SP.emp_no

fname           lname           dept_no     order_value description                                        
--------------- --------------- ----------- ----------- -------------------------------------------------- 
Fred            Goalie          3           7           Toshiba 6700 Pro
Fred            Goalie          3           6           MS Office Professional * 30
Fred            Goalie          3           12          ScanPRO 4800 Scanner
Alan            Brick           1           5           Modems and Cables etc
Fred            Goalie          3           2           Laser printer
Ernest          Flipper         3           27          Complete Desktop Publishing System
Alan            Brick           1           3           SQL Server 2005 20 user licence
Fred            Goalie          3           3           Printer cartridges

--    Now ADD the emp_no of the person who made the sale as a FIRST column 
--    and also sort the sales by this emp_no.

SELECT 	SP.emp_no, fname, lname, dept_no, order_value, description
FROM	sale S INNER JOIN salesperson SP
	ON S.emp_no = SP.emp_no
ORDER BY SP.emp_no

emp_no      fname           lname           dept_no     order_value description                                        
----------- --------------- --------------- ----------- ----------- -------------------------------------------------- 
10          Alan            Brick           1           5           Modems and Cables etc
10          Alan            Brick           1           3           SQL Server 2005 20 user licence
50          Ernest          Flipper         3           27          Complete Desktop Publishing System
60          Fred            Goalie          3           2           Laser printer
60          Fred            Goalie          3           3           Printer cartridges
60          Fred            Goalie          3           12          ScanPRO 4800 Scanner
60          Fred            Goalie          3           7           Toshiba 6700 Pro
60          Fred            Goalie          3           6           MS Office Professional * 30

--p3) Copy/Paste your code from p2).
--    Make 2 changes to your code: 
--    Firstly, add the MANAGER of the seller of the sale as an extra FIRST column.
--    It is now broken, so get this working.

--    Secondly, restrict the answer set so that it lists only those sales
--    that contain the text 'printer' in their description. (2 rows)

--3.1)

SELECT 	manager, SP.emp_no, fname, lname, D.dept_no, order_value, description
FROM 	sale S INNER JOIN salesperson SP
	ON S.emp_no = SP.emp_no
INNER JOIN dept D
	ON D.dept_no = SP.dept_no

manager              emp_no      fname           lname           dept_no     order_value description                                        
-------------------- ----------- --------------- --------------- ----------- ----------- -------------------------------------------------- 
Paul Peach           60          Fred            Goalie          3           7           Toshiba 6700 Pro
Paul Peach           60          Fred            Goalie          3           6           MS Office Professional * 30
Paul Peach           60          Fred            Goalie          3           12          ScanPRO 4800 Scanner
Adam Apricot         10          Alan            Brick           1           5           Modems and Cables etc
Paul Peach           60          Fred            Goalie          3           2           Laser printer
Paul Peach           50          Ernest          Flipper         3           27          Complete Desktop Publishing System
Adam Apricot         10          Alan            Brick           1           3           SQL Server 2005 20 user licence
Paul Peach           60          Fred            Goalie          3           3           Printer cartridges

--3.2)

SELECT 	manager, SP.emp_no, fname, lname, D.dept_no, order_value, description
FROM 	sale S INNER JOIN salesperson SP
	ON S.emp_no = SP.emp_no
INNER JOIN dept D
	ON D.dept_no = SP.dept_no
WHERE 	description LIKE '%printer%'

manager              emp_no      fname           lname           dept_no     order_value description                                        
-------------------- ----------- --------------- --------------- ----------- ----------- -------------------------------------------------- 
Paul Peach           60          Fred            Goalie          3           2           Laser printer
Paul Peach           60          Fred            Goalie          3           3           Printer cartridges


--p4) Display each contact's name and their company's name. (10 rows)
--    Make sure you JOIN on the thing they share!!
--    Note how many contacts (1, 2, 3, 4) are in each company as you will 'count' them in code later. 

SELECT	Cont.name 'Contact', Comp.name 'Company'
FROM 	contact Cont INNER JOIN company Comp
	ON Cont.company_no = Comp.company_no

Contact              Company              
-------------------- -------------------- 
Munching Mike        Happy Heaters PLC
Naughty Nick         Icicle Igloos PLC
Ollie Octopus        Icicle Igloos PLC
Purposeful Peter     Judo Jeans PLC
Quentin Quail        Judo Jeans PLC
Robber Red           Judo Jeans PLC
Marvellous Marvin    Kipper Kickers Inc
Ricky Rambo          Kipper Kickers Inc
Terrible Tim         Kipper Kickers Inc
Uppy Umbrella        Kipper Kickers Inc

-- IF YOU HAVE TIME


--p5) Managers like to telephone contacts after 'big' sales. 
--    It's a sort of 'compliance' requirement.
--    A big sale is defined as one where the value of the order is greater than 50% of the 
--    salesperson's target, i.e. somebody hits half their yearly target in one deal.
--    Produce a list detailing manager, plus the name and telephone number of the contact 
--    that the manager must call.
--    This is a 'long' query but some of the join code has been pasted in for you from p3) 
--    above. 
--    If you are NOT getting a 4 row result set, there are 2 things that MIGHT help you 
--    spot the problem. 
--    a) If you are getting 7 rows, try adding the order_no column to the select list, 
--       rerun and look closely.
--    b) If you get 6 rows, recognize YOU (the contact you inserted earlier) 
--       have not been sold to, and should not be in result set. Why are you there?

SELECT 	manager, C.name, C.tel 
FROM  	dept D
	INNER JOIN salesperson SP
		ON D.dept_no = SP.dept_no
	INNER JOIN sale S
		ON SP.emp_no = S.emp_no
	INNER JOIN contact C
		on  C.company_no   = S.company_no
		AND C.contact_code = S.contact_code
WHERE order_value > .5 * SP.sales_target

manager              name                 tel                       
-------------------- -------------------- ------------------------- 
Paul Peach           Munching Mike        (0207)223-9887           
Paul Peach           Ollie Octopus        0207-341-566670 ext 10   
Adam Apricot         Munching Mike        (0207)223-9887           
Paul Peach           Purposeful Peter     0131 324545 ext 213      

--    Important: When you get this working, can you accurately predict how many rows the result set 
--    will contain when you run it WITHOUT the WHERE clause? I.e. natural JOIN of the 4 tables.
--    Did you guess correctly?

Answer 8 rows, it is driven by the number of rows in 'sale' (the 'many' table)

--    How many rows would you expect to get if you ran your code after these 
--    3 INSERTs happened?

INSERT INTO dept        VALUES (20, 'Dept 20', 'Dept 20 Manager', 20)
INSERT INTO salesperson VALUES (100, 'Pete', 'Pitstop', 2, 5, NULL, NULL, NULL, NULL)
INSERT INTO contact     VALUES (4000, 'ZZ', 'Zinedine Zidane', 'Celebrity', NULL, NULL) 

Answer still 8, adding a dept with no people, a person with no sales, or a contact who has 
       not been sold too will not affect the result of an INNER JOIN with sale


--  Run them (the DELETE statements to remove them are below), and see if you guessed right. 

--    Now decide how many rows you would get if you now ran this statement:

INSERT INTO sale 
VALUES (900, 10, 1000, 'MM', 3, '05-12-2006', 'Metal 3*2 Desk')

Answer 9 as sale table has 9 rows

--    Guessed correctly? If not ask your instructor.


--    Here are the DELETE statements (run them if you ran any of the 4 INSERTs above).

DELETE FROM salesperson WHERE emp_no = 100 
DELETE FROM dept 	WHERE dept_no = 20
DELETE FROM contact     WHERE name = 'Zinedine Zidane'
DELETE FROM sale        WHERE order_no = 900












/***************************************************************************************
**										      **
** 		END OF CHAPTER 5. Practical 1				       	      **
**										      **
****************************************************************************************/



























/***************************************************************************************
**										      **
** 		CHAPTER 5. Practical 2.					  	      **
**		Outer JOINs							      **
** 										      **
****************************************************************************************/
SELECT * FROM dept
SELECT * FROM salesperson
SELECT * FROM sale
SELECT * FROM contact
SELECT * FROM company

--p1) This practical is done largely as a 'tutorial' learning exercise, you practise them  later.
--    Run the following 3 precoded queries.
--    NOTE -  there are 4 depts with 4 managers. (Assuming you completed the DELETE practical earlier) 
--    NOTE -  the salespeople are in 3 different depts. 
--    NOTE -  the 3rd query fails to list 'Diver Dan' because he manages a 'dept' with no people.

SELECT 	dept_no, manager 
FROM dept

SELECT DISTINCT dept_no AS 'Distinct list of depts that people are in'
FROM salesperson

SELECT D.dept_no, manager, lname
FROM salesperson SP INNER JOIN dept D
	                 ON SP.dept_no = D.dept_no

dept_no     manager              
----------- -------------------- 
1           Adam Apricot        
2           Barbara Banana      
3           Paul Peach          
4           Diver Dan           

Distinct list of depts that people are in     
----------------------------------------- 
1
2
3

dept_no     manager              lname           
----------- -------------------- --------------- 
1           Adam Apricot         Brick
2           Barbara Banana       Custard
2           Barbara Banana       Digger
3           Paul Peach           Ernst
3           Paul Peach           Flipper
3           Paul Peach           Goalie


--p2) Copy in the 3rd query from p1) and change the word INNER to the word RIGHT

--    This is called an outer JOIN, inserting the word RIGHT means literally 
--    "include every row from the table on the RIGHT of the word JOIN", 
--    (even if there is no matching row in the table on the LEFT).


SELECT D.dept_no, manager, lname
FROM salesperson SP RIGHT JOIN dept D
	ON SP.dept_no = D.dept_no

dept_no     manager              lname           
----------- -------------------- --------------- 
1           Adam Apricot         Brick
2           Barbara Banana       Custard
2           Barbara Banana       Digger
3           Paul Peach           Ernst
3           Paul Peach           Flipper
3           Paul Peach           Goalie
4           Diver Dan            NULL

-- Now 'Coalesce' the lname column to display 'Nobody in this dept' where appropriate.

SELECT D.dept_no, manager, COALESCE(lname, 'Nobody in this dept') as Surname
FROM salesperson SP RIGHT JOIN dept D
	ON SP.dept_no = D.dept_no

dept_no     manager              Surname             
----------- -------------------- ------------------- 
1           Adam Apricot         Brick
2           Barbara Banana       Custard
2           Barbara Banana       Digger
3           Paul Peach           Ernst
3           Paul Peach           Flipper
3           Paul Peach           Goalie
4           Diver Dan            Nobody in this dept

-- Recognize that

FROM dept D LEFT JOIN salesperson SP
-- would give the same result as

FROM salesperson SP RIGHT JOIN dept D

-- p3) Ask yourself whether this FROM clause makes any sense?

FROM dept D RIGHT JOIN salesperson SP

-- Does this translate into
-- "Show me all the people even one's in a non existent dept"?
-- Hopefully the referential integrity (covered fully later) between the tables
-- will ensure that if you have depts 1-4 only, that there is no one in dept 5 or 6 or 87.

-- But can a salesperson be in no (NULL) dept at all?
-- Well, it depends whether 'dept_no' of 'salesperson' is a NULLable (optional) column or    not.
-- In your schema/table it IS an optional column so the following INSERT (try it) will run    ok.

INSERT INTO salesperson(emp_no, fname, lname, dept_no)
VALUES		       (70,    'Monica', 'Ell', NULL)

(1 row(s) affected)

-- The following INNER JOIN query will not discover her

SELECT manager, COALESCE(lname, 'Nobody in this dept') AS Surname
FROM salesperson SP INNER JOIN dept D
	                 ON SP.dept_no = D.dept_no

manager              Surname             
-------------------- ------------------- 
Adam Apricot         Brick
Barbara Banana       Custard
Barbara Banana       Digger
Paul Peach           Ernst
Paul Peach           Flipper
Paul Peach           Goalie

-- Nor will this Outer JOIN 

SELECT manager, COALESCE(lname, 'Nobody in this dept') AS Surname 
FROM salesperson SP RIGHT JOIN dept D
	                 ON SP.dept_no = D.dept_no

manager              Surname             
-------------------- ------------------- 
Adam Apricot         Brick
Barbara Banana       Custard
Barbara Banana       Digger
Paul Peach           Ernst
Paul Peach           Flipper
Paul Peach           Goalie
Diver Dan            Nobody in this dept

-- But this one will, note dept D RIGHT JOIN salesperson SP 

SELECT COALESCE(manager,'Has no manager') AS Manager, lname
FROM dept D RIGHT JOIN salesperson SP
	         ON SP.dept_no = D.dept_no

Manager              lname           
-------------------- --------------- 
Adam Apricot         Brick
Barbara Banana       Custard
Barbara Banana       Digger
Paul Peach           Ernst
Paul Peach           Flipper
Paul Peach           Goalie
Has no manager       Ell

-- p4) �Can I do a LEFT & a RIGHT JOIN at the same time?� is a question often asked.
--     Yes, it�s called a FULL JOIN.

SELECT COALESCE(manager, '** Has no manager **')      AS Manager, 
       COALESCE(lname,   '** Nobody in this dept **') AS Surname
FROM dept D FULL JOIN salesperson SP
	         ON SP.dept_no = D.dept_no

Manager              Surname                   
-------------------- ------------------------- 
** Has no manager ** Ell
Adam Apricot         Brick
Barbara Banana       Custard
Barbara Banana       Digger
Paul Peach           Ernst
Paul Peach           Flipper
Paul Peach           Goalie
Diver Dan            ** Nobody in this dept **

-- But if you were now to include 'dept_no' in this query, should you choose
-- SP.dept_no or D.dept_no, because one of the rows is surely going to have a NULL dept_no?
-- Well you could include either and COALESCE it to remove the NULL, but there is one    problem.
-- Namely, the 'COALESCE' function requires that both parameters are of the same data type,
-- so 

COALESCE(D.dept_no, 0 )    -- would be valid syntax but produce a misleading '0' in results.
COALESCE(D.dept_no, 'n/a') -- would fail as 1st arg is numeric, but 2nd arg is not.

-- So the solution would be to convert the 'dept_no' into a character string.
-- In SQL Server it would be   

COALESCE(STR(D.dept_no, 2), 'n/a')   -- meaning convert D.dept_no into a 2 character string.

-- In Oracle it would be 
NVL(TO_CHAR(D.dept_no, '99'), 'n/a')
-- or
COALESCE(TO_CHAR(D.dept_no, '99'), 'n/a')


SELECT COALESCE(STR(D.dept_no,2),'N/A') AS Dept_no, 
       COALESCE(manager,'** Has no manager **') AS Manager, 
       COALESCE(lname,'** Nobody in this dept **') AS Surname
FROM dept D FULL JOIN salesperson SP
	         ON SP.dept_no = D.dept_no

Dept_no Manager              Surname                   
------- -------------------- ------------------------- 
N/A     ** Has no manager ** Ell
 1      Adam Apricot         Brick
 2      Barbara Banana       Custard
 2      Barbara Banana       Digger
 3      Paul Peach           Ernst
 3      Paul Peach           Flipper
 3      Paul Peach           Goalie
 4      Diver Dan            ** Nobody in this dept **

-- p5) The odd thing is, what happens when you add the following 'WHERE' clause to 
--     this 'Left' JOIN.

SELECT D.dept_no, 
       COALESCE(manager,'** Has no manager **') AS Manager, 
       COALESCE(lname,'** Nobody in this dept **') AS Surname
FROM   dept D LEFT JOIN salesperson SP
	           ON SP.dept_no = D.dept_no
WHERE  SP.dept_no IS NULL

dept_no     Manager              Surname                   
----------- -------------------- ------------------------- 
4           Diver Dan            ** Nobody in this dept **

(1 row(s) affected)

-- You have just seen a technique (there are others, as we will see) of how to find the 
-- depts that have no salespeople by JOINing Dept to Salesperson, 
-- including all the 'spares', and then just retaining the 'spares'.
-- Later in the course you will see 2 other ways of finding 'Depts with no People'. 

-- The mistake you must not make is to write this 'WHERE' clause:

WHERE SP.post_code IS NULL          -- Why?

-- Because then you will be listing managers of depts (and their employees) that EITHER
-- a) have no people in them, or
-- b) have a person, but a person who has no post_code.

SELECT D.dept_no, 
       COALESCE(manager,'** Has no manager **') AS Manager, 
       COALESCE(lname,'** Nobody in this dept **') AS Surname
FROM   dept D LEFT JOIN salesperson SP
	           ON SP.dept_no = D.dept_no
WHERE  SP.post_code IS NULL  -- no good, must be a primary key(not NULL) column for safety

dept_no     Manager              Surname                   
----------- -------------------- ------------------------- 
3           Paul Peach           Ernst
3           Paul Peach           Goalie
4           Diver Dan            ** Nobody in this dept **

(3 row(s) affected)

-- p6) Clean up, DELETE that 7th employee you added  
DELETE FROM salesperson WHERE emp_no = 70

-- NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE ************

-- What you have just learnt is ANSI 92 Outer JOIN Syntax. 
-- LEFT, RIGHT, FULL mean LEFT OUTER, RIGHT OUTER, FULL OUTER respectively.
-- Your DBMS may allow you to leave out the word 'OUTER'; SQL Server does, Access doesn�t.

-- So what about pre 1992 Outer JOINs?
-- Well there was no standard way of doing it, but nearly all the databases give support 
-- for some syntax that achieved it.

-- e.g. Sybase and Microsoft SQL Server
SELECT D.dept_no, 
       COALESCE(manager,'Has no manager') AS Manager, 
       COALESCE(lname,'Nobody in this dept') AS Surname
FROM   dept D, salesperson SP
WHERE  D.dept_no *= SP.dept_no

-- i.e. put the '*' on the left of the '=' to force a LEFT JOIN. It is still supported.

-- The Oracle equivalent, and Oracle was very slow (Oracle 9i) to support the new syntax, 
-- is:
SELECT D.dept_no, 
       NVL(manager,'Has no manager') AS Manager, 
       NVL(lname,'Nobody in this dept') AS Surname
FROM   dept D, salesperson SP
WHERE  D.dept_no = SP.dept_no(+)    -- i.e put a (+) on the RIGHT to do a LEFT JOIN !!!

-- as in 'add'(+) an 'imaginary' row to this table to force a match with the other (reasonable!).

/***************************************************************************************
**										      **
** 		END OF CHAPTER 5. Practical 2.			         	      **
**										      **
****************************************************************************************/
























/***************************************************************************************
**										      **
** 		CHAPTER 6. Practical 1.					  	      **
**		CREATE/ALTER Table Statement					      **
** 										      **
****************************************************************************************/

-- This practical requires writing NO code.

--p1) Run the following syntax to CREATE a 'review' table:

CREATE TABLE review 
(
   emp_no 		INTEGER 	NOT NULL,
   review_date 	        DATE    	NOT NULL,      
   grade 		CHAR(1) 	NULL
)

-- Try running it again.





--p2) Add a 4th column called 'notes'; it is to be a character column of varying length, not
--    to exceed 40 characters in length and is to allow NULLs.

ALTER TABLE review 
ADD notes VARCHAR(40) NULL

-- Try running it again.






--p3) Inspect the table structure within the system tables. (SQL SERVER specific code supplied)

sp_help review 



--p4) INSERT two rows into the table (code supplied). The values are duplicated, but it will
--    run quite succesfully because the 'review' table has no primary key defined and 
--    therefore no uniqueness is being enforced.

INSERT INTO review         
VALUES	(23, '05-12-2006', 'A','He is happy')

INSERT INTO review           
VALUES	(23, '09-30-2006', 'A','He is still happy')








--p5) Display the data in the table. (code supplied)

SELECT 	*
FROM 	review

emp_no      review_date                                            grade notes                                    
----------- ------------------------------------------------------ ----- ---------------------------------------- 
23          2006-05-12 00:00:00.000                                A     He is happy
23          2006-09-30 00:00:00.000                                A     He is still happy

-- So 2 reviews for employee 23 have been added, but employee 23 does not exist in 
-- salesperson. Your database is lacking in 'referential integrity', because we never told 
-- the DBMS about any relationship between emp_no of 'review' and emp_no of 'salesperson'.
-- We will shortly do something about that!


/***************************************************************************************
**										      **
** 		END OF CHAPTER 6. Practical 1.				              **
**										      **
****************************************************************************************/



















































/***************************************************************************************
**										      **
** 		CHAPTER 6. Practical 2.					  	      **
**		Defining Primary Keys and Foreign Keys using DRI, 			      **
**		and observing INSERT/UPDATE/DELETE behaviour			      **
** 										      **
****************************************************************************************/



--p1) 	Drop your 'review' table by running this code
	
DROP TABLE review

--p2)   Now we will reCREATE 'review' table this time with some additional constraints.

--      There is a composite Primary Key constraint defined on the 'emp_no' and 'review_date' 
--      columns. The constraint is a separate object in the database which can be 'dropped' 
--      independently of the table itself, so it is a good idea to give it a meaningful name 
--      like 'pk_review'. 

CONSTRAINT pk_review PRIMARY KEY(emp_no, review_date)

--  There is a Foreign Key constraint defined on 'emp_no' which basically says
--  "Values in the 'emp_no' column of 'review' should exist in the primary key column 
--  of 'salesperson' which just happens to also be called 'emp_no' but doesn�t have to be".

CONSTRAINT fk_salesperson_link FOREIGN KEY(emp_no) REFERENCES salesperson

--  Now run this CREATE table syntax and then observe the changed behaviour you get when you
--  either INSERT/UPDATE 'reviews' or DELETE/UPDATE 'salespersons'.

--  Run this code:	

	CREATE TABLE review
		(
	emp_no		INTEGER 	NOT NULL,
	review_date 	DATETIME	NOT NULL,
	grade		CHAR(1)	NULL,
	CONSTRAINT pk_review PRIMARY KEY(emp_no, review_date),
	CONSTRAINT fk_salesperson_link FOREIGN KEY(emp_no)  REFERENCES salesperson
		)



--p3) Run all the following code samples one by one observing the output and explaining
--    briefly, �why?�.
--    First, try to add a row to the review table, for employee 40. (code supplied)

INSERT INTO 	review
VALUES		(40, '05-12-2006', 'A')

-- What happened and why?
-- It worked but only because 'emp_no=40' does exist in 'salesperson'.





--p4) Try to add a row for employee 90, who does not exist as a salesperson. (code supplied)

INSERT INTO 	review
VALUES		(90, '05-12-2006', 'A')

-- What happened and why?
INSERT statement conflicted with COLUMN FOREIGN KEY constraint 'fk_salesperson_link'. 
The conflict occurred in database 'xxxxxxxxxx', table 'salesperson', column 'emp_no'.
The statement has been terminated.

-- INSERT failed





--p5) The following statements all violate the referential integrity between the 2 tables.

UPDATE review
SET    emp_no = 41 
WHERE  emp_no = 40 

DELETE FROM salesperson
WHERE  emp_no = 40

UPDATE salesperson
SET    emp_no = 41
WHERE  emp_no = 40

-- all fail for same reason.




--p6) Decide what should happen from 2 suggested answers if you were to run the next   statement.

DELETE FROM dept    -- NOTE no WHERE clause!!

-- To remind you: Dept nos are currently 1, 2, 3 & 4 but there are no people in dept 4.
-- So should it therefore ?

-- a) DELETE just dept number 4 and leave the other 3 depts intact so as not to orphan 
--    the employees in them, or

-- b) do nothing, as it cannot achieve everything that you intended to do in this 
--    statement.


-- Try it

-- But, 

DELETE FROM sale

-- would run just fine as nothing depends on a sale.
-- The following statements will reinstate your sale table if you run the 'DELETE FROM sale'.

INSERT INTO sale VALUES(100, 60, 1000, 'MM',  7.00, '06-24-2006', 'Toshiba 6700 Pro' )
INSERT INTO sale VALUES(200, 60, 3000, 'QQ',  6.00, '05-01-2006', 'MS Office Professional * 30' )
INSERT INTO sale VALUES(300, 60, 2000, 'OO', 12.00, '07-14-2006', 'ScanPRO 4800 Scanner' )
INSERT INTO sale VALUES(400, 10, 1000, 'MM',  5.00, '08-09-2006', 'Modems and Cables etc' )
INSERT INTO sale VALUES(500, 60, 4000, 'TT',  2.00, '07-23-2006', 'Laser printer' )
INSERT INTO sale VALUES(600, 50, 3000, 'PP', 27.00, '05-23-2006', 'Complete Desktop Publishing System' )
INSERT INTO sale VALUES(700, 10, 2000, 'OO',  3.00, '01-23-2006', 'SQL Server 2007 20 user licence' )
INSERT INTO sale VALUES(800, 60, 3000, 'RR',  3.00, '11-15-2006', 'Printer cartridges' )



/***************************************************************************************
**										      **
** 		END OF CHAPTER 6. Practical 2.				              **
**										      **
****************************************************************************************/

































/***************************************************************************************
**										      **
** 		CHAPTER 7. Practical 1.					  	      **
**		VIEWs.					 			      **
**										      **
****************************************************************************************/
sp_help company

--p1)  CREATE a VIEW named London.
--     It is going to show rows and columns from the 'company' table.
--     It should show company number, name and county columns.
--     It should only include rows in the London area (County = 'London'). 
--     When used for data entry it should NOT ALLOW non-London companies to be added.
--     Be a developer and CREATE the view.

CREATE VIEW London 
AS 
	SELECT 	company_no, name, county 
	FROM 	company
        WHERE 	county = 'London'
        WITH CHECK OPTION

--p2)  Now be the 'user' and SELECT everything (all rows and all columns) 
--     FROM the newly created VIEW.  
--     (3 rows, with 3 columns each)

SELECT 	*
FROM	London

company_no  name                 county          
----------- -------------------- --------------- 
1000        Happy Heaters PLC    London
2000        Icicle Igloos PLC    London
3000        Judo Jeans PLC       London

-- p3)  Copy/paste the code from 1) and
--	     alter the VIEW to add the 'post_code' column to the VIEW so that it appears 
--      before 'county'.

ALTER VIEW London 
AS 
	SELECT 	company_no, name, post_Code, county 
	FROM 	company
        WHERE 	county = 'London'
        WITH CHECK OPTION

--p4)  Now SELECT from the VIEW again as you did in 2) above, but this time
--     only include 'name' and 'post_code' columns.
--     Include a 'WHERE' clause that only SELECTs rows from the VIEW if 
--     the post_code starts with an 'N'.
--     Order the rows by 'post_code'.

SELECT 	name, post_code
FROM	London
WHERE   post_code LIKE 'N%'
ORDER BY post_code

name                 post_code  
-------------------- ---------- 
Icicle Igloos PLC    N1 4LH    
Judo Jeans PLC       N9 2FG    

--p5)  Try to run this INSERT statement of a company based in 'Essex'. It shouldn�t be  allowed! 
--     If it is successful, you have forgotten 3 key words from your CREATE/ALTER syntax.

INSERT INTO london
VALUES	(5000, 'ABC CO','RF45 7GH', 'ESSEX')

"The attempted INSERT or UPDATE failed because the target VIEW either specifies 
WITH CHECK OPTION or spans a VIEW that specifies WITH CHECK OPTION and one or more 
rows resulting from the operation did not qualify under the CHECK OPTION constraint.
The statement has been terminated. (SQL Server Message)"

-- The INSERT fails because the data violates the With Check Option condition.

-- If your INSERT did run successfully use this DELETE statement to remove the row.

DELETE FROM company WHERE company_no = 5000

/***************************************************************************************
**										      **
** 		END OF CHAPTER 7. VIEWs					              **
**										      **
****************************************************************************************/












































/***************************************************************************************
**										      **
** 		CHAPTER 8. Practical 1.					  	      **
**		Aggregates and summarised queries				      **
**										      **
****************************************************************************************/

SELECT * FROM dept
SELECT * FROM salesperson
SELECT * FROM sale
SELECT * FROM contact
SELECT * FROM company


--p1) Display the sum and average of the sales_targets as well as a count 
--    of the number of salespeople. (1 row 3 columns!)

SELECT 	SUM(sales_target) Total,
	AVG(sales_target) Average,
	COUNT(*) 'No of sales people'
FROM	salesperson

Total            Average             No of sales people 
---------------- ------------------- ------------------ 
66.00            11.000000           6

--p2) Copy/Paste 1) and amend to display the same columns per dept number. 
--    Ensure that you also include dept_no at the start of the SELECT list (3 rows, 4   columns).

SELECT 	dept_no,
	SUM(sales_target) Total,
	AVG(sales_target) Average,
	COUNT(*) 'No of sales people'
FROM	salesperson
GROUP BY dept_no

dept_no     Total           Average                No of sales people 
----------- --------------- ---------------------- ------------------ 
1           9.00            9.000000               1
2           21.00           10.500000              2
3           36.00           12.000000              3

--p3) Users of the output from 2) say "great report but I am not very good on dept numbers 
--    can you include dept names AS WELL, and I would like the dept name to appear 
--    just to the right of the dept_no".
--    So copy/paste from 2), add dept_name (now it is broken), then repair it.

SELECT 	SP.dept_no,
	dept_name,
	SUM(SP.sales_target) Total,
	AVG(SP.sales_target) Average,
	COUNT(*) 'No of sales people'
FROM	salesperson SP JOIN dept D
	ON SP.dept_no = D.dept_no
GROUP BY SP.dept_no, dept_name

dept_no     dept_name            Total          Average             No of sales people 
----------- -------------------- -------------- ------------------- ------------------ 
1           Animal Products      9.00           9.000000            1
2           Business Systems     21.00          10.500000           2
3           Credit Control       36.00          12.000000           3

-- Do NOTE!
-- All you have done is added 1 column to the display. How much did you have to change?
-- welcome to SQL!

/***************************************************************************************
**										      **
** 		END OF CHAPTER 8. Practical 1				              **
**										      **
****************************************************************************************/































/***************************************************************************************
**										      **
** 		CHAPTER 8. Practical 2.					  	      **
**		More Aggregates and summarised queries				      **
**										      **
****************************************************************************************/

--p1) Look at your output for part 3) of the previous lab and answer this question. 
--    Which depts (names) have more than 1 person?
--    Copy/paste your code from part 3) above and amend it to show just the answer to that 
--    question (2 rows, 1 column).  You do need to use your DELETE key in this one!

SELECT 	dept_name
FROM	salesperson SP JOIN dept D
	ON SP.dept_no = D.dept_no
GROUP BY dept_name
HAVING 	COUNT(*) > 1

dept_name            
-------------------- 
Business Systems    
Credit Control


--p2) Read this carefully, for a new query. 
--    Display a COUNT of the number of contacts per company name (4 rows).
--    You might like to run the code that you wrote yesterday in Chapter 5 Practical 1 P4) (JOINs),
--    here it is, to help you picture what your output is going to look like.

SELECT	CONT.name 'Contact', COMP.name 'Company'
FROM 	contact CONT INNER JOIN company COMP
	                   ON CONT.company_no = COMP.company_no

Contact              Company              
-------------------- -------------------- 
Munching Mike        Happy Heaters PLC
Naughty Nick         Icicle Igloos PLC
Ollie Octopus        Icicle Igloos PLC
Purposeful Peter     Judo Jeans PLC
Quentin Quail        Judo Jeans PLC
Robber Red           Judo Jeans PLC
Marvellous Marvin    Kipper Kickers Inc
Ricky Rambo          Kipper Kickers Inc
Terrible Tim         Kipper Kickers Inc
Uppy Umbrella        Kipper Kickers Inc


SELECT 	COMP.name, count(*) 'TotalContacts'
FROM 	company COMP JOIN contact CONT
	ON COMP.company_no = CONT.company_no
GROUP BY COMP.name

name                 TotalContacts 
-------------------- ------------- 
Happy Heaters PLC    1
Icicle Igloos PLC    2
Judo Jeans PLC       3
Kipper Kickers Inc   4



-- p3) You�re lucky that at the moment all companies have contacts,
--     but in the future some companies may exist (in 'company') to which we have not yet 
--     allocated any 'contacts'. 
--     Copy/Paste your code from p2) and ensure that those 'Companies without contacts' 
--     also get included in the report.
--     To ensure that your output now changes, run this INSERT first.

INSERT INTO company 
VALUES( 5000, 'ABC Ltd(no contacts)',	 '(01456)346782', 'Dorset' , 'ST8 3RG' )
 
SELECT 	COMP.name, count(*) 'TotalContacts'
FROM 	company COMP LEFT JOIN contact CONT
	ON COMP.company_no = CONT.company_no
GROUP BY COMP.name

name                 TotalContacts 
-------------------- ------------- 
ABC Ltd(no contacts) 1
Happy Heaters PLC    1
Icicle Igloos Inc    2
Judo Jeans PLC       3
Kipper Kickers Inc   4

-- Did the �TotalContacts� correctly display as '0', or did it say '1'?
-- If '1' then solve your problem.

SELECT 	COMP.name, count(CONT.company_no) 'TotalContacts'
FROM 	company COMP LEFT JOIN contact CONT
	ON COMP.company_no = CONT.company_no
GROUP BY COMP.name

name                 TotalContacts 
-------------------- ------------- 
ABC Ltd(no contacts) 0
Happy Heaters PLC    1
Icicle Igloos Inc    2
Judo Jeans PLC       3
Kipper Kickers Inc   4

--p4) CREATE a VIEW called 'NoOfContacts' that contains the SELECT statement of 5).

CREATE VIEW NoOfContacts 
AS
SELECT 	COMP.name, COUNT(CONT.company_no) 'TotalContacts'
FROM 	company COMP LEFT JOIN contact CONT
	ON COMP.company_no = CONT.company_no
GROUP BY COMP.name

--p5) Be the 'user' who uses the VIEW from 4). 
--    ORDER BY descending number of contacts.

SELECT  * 
FROM 	NoOfContacts
ORDER BY TotalContacts desc

name                 TotalContacts 
-------------------- ------------- 
Kipper Kickers Inc   4
Judo Jeans PLC       3
Icicle Igloos Inc    2
Happy Heaters PLC    1
ABC Ltd(no contacts) 0

-- Recognise how little the 'user' needs to know of SQL, compared to the person 
-- who wrote the underlying SQL.




-- Clean up:
DELETE FROM company WHERE company_no = 5000










--p6) Display just the literal 
--    'YES, total sales exceeds 50, hurrah!',
--    but only if it�s true!!

SELECT 'YES, total sales exceeds 50, hurrah!' AS Message 
FROM 	sale
HAVING 	SUM(order_value) > 50


Message                              
------------------------------------ 
YES, total sales exceeds 50, hurrah!



-- If you are finished, here is some extra stuff to look at.




--  p7) TUTORIAL PORTION
--      ================
--  For your consideration, NO CODE NEEDS TO BE WRITTEN HERE. 

SELECT company_no, emp_no, SUM(order_value) 'Total sales'
FROM sale
GROUP BY company_no, emp_no
ORDER BY company_no, emp_no

--  produces this standard summarised report.

company_no  emp_no      Total sales 
----------- ----------- ----------- 
1000        10          5
1000        60          7
2000        10          3
2000        60          12
3000        50          27
3000        60          21
4000        60          2

-- SQL Server/Sybase/Oracle/Access all support variations on the following syntax.

-- The ROLLUP clause
-- =================

SELECT company_no, emp_no, SUM(order_value) 'Total sales'
FROM sale
GROUP BY company_no, emp_no WITH ROLLUP  -- rolling up to higher totals like a balance sheet

-- produces
company_no  emp_no      Total sales 
----------- ----------- ----------- 
1000        10          5			<-- this row was in result set above
1000        60          7			<-- this row was in result set above
1000        NULL        12		<-- extra total row for company_no 1000
2000        10          3			<-- this row was in result set above
2000        60          12			<-- this row was in result set above
2000        NULL        15		<-- extra total row for company_no 2000
3000        50          27			<-- this row was in result set above
3000        60          21			<-- this row was in result set above
3000        NULL        48		<-- extra total row for company_no 3000
4000        60          2			<-- this row was in result set above
4000        NULL        2		<-- extra total row for company_no 4000
NULL        NULL        77		<-- extra grand total row for all companies 


-- and

-- the CUBE clause
-- =============== 

SELECT company_no, emp_no, SUM(order_value) 'Total sales'
FROM sale
GROUP BY company_no, emp_no WITH CUBE  -- rolling up company_no totals and showing emp_no totals

-- produces

company_no  emp_no      Total sales 
----------- ----------- ----------- 
1000        10          5
1000        60          7
1000        NULL        12		<-- total row for company_no 1000
2000        10          3
2000        60          12
2000        NULL        15		<-- total row for company_no 2000
3000        50          27
3000        60          21
3000        NULL        48		<-- total row for company_no 3000
4000        60          2
4000        NULL        2		<-- total row for company_no 4000		
NULL        NULL        77		<-- grand total row for all companies
NULL        10          8		<-- extra total row for emp_no 10
NULL        50          27		<-- extra total row for emp_no 50
NULL        60          42		<-- extra total row for emp_no 60

-- So with a little coalescing,

SELECT COALESCE(STR(company_no,4),'All Companies  ') AS company_no, 
       COALESCE(STR(emp_no,3),'All employees  ') AS emp_no,
       SUM(order_value) 'Total sales'
FROM   sale
GROUP BY company_no, emp_no WITH CUBE  -- rolling up company_no totals and showing emp_no totals

-- we can get 

company_no      emp_no          Total sales 
--------------- --------------- ----------- 
1000             10             5
1000             60             7
1000            All employees   12
2000             10             3
2000             60             12
2000            All employees   15
3000             50             27
3000             60             21
3000            All employees   48
4000             60             2
4000            All employees   2
All Companies   All employees   77                 -- Grand total line from rollup
All Companies    10             8		   --|
All Companies    50             27                 --|these 3 lines because of CUBE
All Companies    60             42		   --|




-- GROUPING SECTION
-- ================

-- The grouping function is useful as it returns '1' if it is  'super-aggregate' row
-- and '0' if it is a normal total row.
-- This is useful if there are 'NULL' values in the original data because now you are not
-- sure whether the NULL is from the data or a 'NULL' is generated in a super-aggregate row
-- because you are using rollup or cube. 

SELECT COALESCE(STR(company_no,4),'All Companies  ') AS company_no, 
       COALESCE(STR(emp_no,3),'All employees  ') AS emp_no,
       SUM(order_value) 'Total sales',
	GROUPING(company_no) AS Grp_CompNo,
	GROUPING(emp_no) AS Grp_EmpNo
FROM sale
GROUP BY company_no, emp_no WITH CUBE  -- rolling up company_no totals and showing emp_no totals

company_no      emp_no          Total sales Grp_CompNo Grp_EmpNo 
--------------- --------------- ----------- ---------- --------- 
1000             10             5           0          0
1000             60             7           0          0        -- normal summarised row both 0
1000            All employees   12          0          1	-- 1 = 'total for all employees'
2000             10             3           0          0
2000             60             12          0          0
2000            All employees   15          0          1        -- 1 = 'total for all employees'
3000             50             27          0          0
3000             60             9           0          0
3000            All employees   36          0          1	-- 1 = 'total for all employees'
4000             60             2           0          0
4000            All employees   2           0          1	-- 1 = 'total for all employees'
All Companies   All employees   65          1          1        -- both '1' as it is grand total
All Companies    10             8           1          0
All Companies    50             27          1          0	-- 1 = 'total for all companies'
All Companies    60             30          1          0

-- Grouping in conjunction with a Case statement enables the following
SELECT 
	-- to determine what appears in 1st column 
	CASE
		WHEN GROUPING(company_no) = 1 AND GROUPING(emp_no) = 1 
					THEN 'Grand Total'	     --Total of both
	ELSE
		CASE
			WHEN GROUPING(company_no) = 1                --But Grouping(emp_no) = 0
					THEN 'All Companies  '       --Total for one employee
			ELSE STR(company_no,4)			     --Normal Total line
		END
	END					 AS Company, -- end of column 1
	-- to determine what appears in 2nd column
	CASE
		WHEN GROUPING(company_no) = 1 AND GROUPING(emp_no) = 1 
					THEN ''			      --Total of both
	ELSE
		CASE
			WHEN GROUPING(emp_no) = 1
					THEN 'All Employees  '        --Total for one company
			ELSE STR(emp_no,2)			      --Normal Total line
		END
	END					 AS Employee, -- end of column 2

	SUM(order_value) AS 'Total sales'
	
FROM sale
GROUP BY company_no, emp_no WITH CUBE  -- rolling up company_no totals and showing emp_no totals

-- produces this elegant output

Company         Employee        Total sales 
--------------- --------------- ----------- 
1000            10              5
1000            60              7
1000            All Employees   12
2000            10              3
2000            60              12
2000            All Employees   15
3000            50              27
3000            60              21
3000            All Employees   48
4000            60              2
4000            All Employees   2
Grand Total                     77
All Companies   10              8
All Companies   50              27
All Companies   60              42

-- There is little variation in this syntax between the major players.



/***************************************************************************************
**										      **
** 		END OF CHAPTER 8. Practical 2				              **
**										      **
****************************************************************************************/
















































/***************************************************************************************
**										      **
** 		CHAPTER 9. Practical 1.					  	      **
**		Standard Subqueries			 			      **
**										      **
****************************************************************************************/
SELECT * FROM dept
SELECT * FROM salesperson
SELECT * FROM sale
SELECT * FROM contact
SELECT * FROM company


--p1) Show the full name of the salesperson(s) (could be more than one) 
--    who has/have the largest sales target. 
--    (Answer - Billy Custard)

SELECT 	fname, lname
FROM 	salesperson 
WHERE 	sales_target =
        (
	SELECT 	MAX(sales_target) 
	FROM 	salesperson
	)

fname           lname              
--------------- ---------------  
Billy           Custard         

-- Question, was it safe to say '=' instead of 'IN'? Why?




--p2) We would like to write a query that displays the number '3'.
--    Why? Because that is the answer to "How many people have sold?".

--    Before starting, run the two queries supplied below, and work out the result (3) manually. 
--    We think there are only 2 ways of doing it. See if you can work out the 2 ways.  

--    Then try to write SQL that mimics exactly what you just did manually.
--    It can be done using a simple subquery. 
--    You may think of a way of doing it without a subquery.
--    Don't try any sort of JOIN!

SELECT emp_no AS 'emp nos of the people' FROM salesperson
SELECT emp_no AS 'emp nos who have sold' FROM sale ORDER BY emp_no


-- The answer is 3 so surely either coded solution query must start with:
SELECT 	COUNT(?)   -- you complete the rest.
FROM    salesperson
WHERE 	emp_no IN
	(
	SELECT emp_no
	FROM   sale
	)

--OR
SELECT COUNT(DISTINCT emp_no)
FROM   sale 








/***************************************************************************************
**										      **
** 		END OF CHAPTER 9. Practical 1.			        	      **
**										      **
****************************************************************************************/































/***************************************************************************************
**										      **
** 		CHAPTER 9. Practical 2.					  	      **
**		Slightly harder subqueries and WHERE EXISTS			 			      **
**										      **
****************************************************************************************/

--p1) Display firstname, lastname, sales_target and the total of the sales(order_value) each 
--    salesperson has achieved.
--    We should ensure every salesperson is in the report not just the 3 who have sold.
--    There should be 1 row in the output for each salesperson (so 6 rows & 4 columns).
 
--    You know how to do this already, nothing new here, so to save time we are going
--    to build up the query together

--    The SELECT list is surely
SELECT	fname,	lname,	sales_target,	SUM(order_value) 'Sales achieved'

--    It clearly cannot run without a GROUP BY, lets use Copy/Paste to complete the GROUP BY
SELECT	fname,	lname,	sales_target,	SUM(order_value) 'Sales achieved'
GROUP BY fname,	lname,	sales_target


--    What's missing? A FROM clause. We need 2 tables joined and we want all salespeople,
--    not just the 3 who have sold so we must do an Outer join.
--    We�ve already covered Joins & Outer Joins, so complete the following code fragment by 
--    keeping/removing the word 'LEFT' or the word 'RIGHT' in the appropriate place and 
--    write the correct ON clause.

SELECT	fname,	lname,	sales_target,	sum(order_value) 'Sales achieved'
FROM salesperson SP LEFT JOIN sale S        -- type 'LEFT' or 'RIGHT' you choose!
	ON SP.emp_no = S.emp_no 
GROUP BY fname,	lname,	sales_target 

--    Ensure you now have 6 rows 4 columns, note the NULL's in the last column
fname           lname           sales_target   Sales achieved 
--------------- --------------- -------------- -------------- 
Alan            Brick           9.00           8
Billy           Custard         14.00          NULL
Chris           Digger          7.00           NULL
Dick            Ernst           11.00          NULL
Ernest          Flipper         12.00          27
Fred            Goalie          13.00          30


-- p2) Use the COALESCE function to ensure that the total sales of the 3 people 
--     who have not sold is 0 and not NULL. 

SELECT	fname,	lname,	sales_target,	COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	ON SP.emp_no = S.emp_no
GROUP BY fname,	lname,	sales_target 

fname           lname           sales_target   Sales achieved 
--------------- --------------- -------------- -------------- 
Alan            Brick           9.00           8
Billy           Custard         14.00          0
Chris           Digger          7.00           0
Dick            Ernst           11.00          0
Ernest          Flipper         12.00          27
Fred            Goalie          13.00          30


--p3) Let's now assess who would be interested in seeing this output.
--    The sales manager / sales director?
--    What would he/she be MOST interested in?
--    They would definitely be interested in how sales_target compares to sales achieved 
--    for each individual so that bonuses / warnings could be issued as appropriate,
--    but even more interested in "does the total sales achieved by all 6 exceed the total
--    of the sales_targets of all 6?" - because that is probably what the sales manager's bonus 
--    is based on!!

--    You have been taught that WHERE EXISTS is used when you want to make an outer query run 
--    or not run based on whether a subquery produces any output.
--    We know that the sum of the targets of the 6 people is 66.
--    We also know that the sum of the order values of the 8 sales is currently 65.
--    So the following query would run. Check it.

SELECT	fname,	lname,	sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	                     ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(
	SELECT 'goobledegook'
	WHERE  66 > 65
	)
GROUP BY fname, lname, sales_target

--    Change the 65 to 67 (mimicking an extra sale of 2) and it won't run.

--    Obviously what we have to do is get rid of the hard coded numbers 65 & 66 and 
--    write SQL that calculates the correct ongoing values, 
--    so that the report is only produced if this statement is true
--    "the sum of the sales targets of all the sales people combined is greater than 
--    the total value of all the orders in sale",
--    
--    i.e. the salespeople as a WHOLE have not hit their COMBINED target.
--    This query is NOT based on any individual or dept but on the sum of everyone!

--    So, work on the above supplied query and ALL your work/typing should be INSIDE the nested
--    inner query as the outer query is already perfect

SELECT	fname,	lname,	sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	                    ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(SELECT sum(sales_target) 
	 FROM salesperson
	 HAVING sum(sales_target) >
		(SELECT sum(order_value)
		 FROM sale
		)
	)
GROUP BY fname, lname, sales_target





















-- This code following is INCORRECT as it double/triple... counts sales targets in the subquery

SELECT	fname, lname, sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	                     ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(SELECT	'Whatever' 
	 FROM 	salesperson SP1 JOIN sale S1          -- MUST NEVER JOIN 1 to MANY AND
                ON SP1.emp_no = S1.emp_no 		      -- 
	 HAVING SUM(sales_target) > SUM(order_value)  -- USE A SUM FROM THE ONE!!!
	)

GROUP BY fname,	lname, sales_target












--p4) Now that we have it working, let�s decide that if the report is produced then the person 
--    who sees it only needs to see the rows representing the salespeople who have 
--    NOT hit target (4 of the 6).

--    Copy the code of 3) down and make a small change to the solution to reduce the answer set 
--    to 4 rows as only 2 sales people have hit their target.

--    Note: you do not need to change any code you have written, just add some more!!
--    Remember there is nothing that is equal to NULL, so in a similar fashion nothing is ever 
--    'greater than' or 'less than' NULL!!  

SELECT	fname,	lname,	sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	                     ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(SELECT SUM(sales_target) 
	 FROM salesperson
	 HAVING SUM(sales_target) >
		(SELECT SUM(order_value)
		 FROM sale
		)
	)
GROUP BY SP.emp_no, fname, lname, sales_target
HAVING sales_target > COALESCE(SUM(order_value),0)    -- line added here


fname           lname           sales_target   Sales achieved 
--------------- --------------- -------------- -------------- 
Alan            Brick           9.00           8
Billy           Custard         14.00          0
Chris           Digger          7.00           0
Dick            Ernst           11.00          0









-- p5) The following line from the result set catches your eye
fname           lname           sales_target   Sales achieved 
--------------- --------------- -------------- -------------- 
Alan            Brick           9.00           8


-- What business decision can you sensibly make given that you know
-- what the underlying SQL is doing?
-- Is it safe to assume Alan is just under target and will exceed it with the next sale?
 
-- What you must consider is this: If there are 2 salespeople both called Alan Brick,
-- will they get 
-- a) a row each in the output 
--    or
-- b) be put in the same bucket, giving us one row in the output representing 
--    all people called 'Alan Brick'?

-- Please decide now. Make a decision. Don't worry if it is wrong.
-- Only then scroll down a page to see the answer.


































-- The answer is neither!!
-- If two salespeople called Alan Brick had the SAME sales_target then they would be treated as 
-- 1 person and there would be one 'Alan Brick' row showing the result of their combined performance
-- Remember the grouping is by fname, lname, & SALES_TARGET, not just by name(s).

-- The problem is that the query does not contain emp_no.
-- You might say it does, but it doesn�t. It's appearance in the ON clause to make 
-- the correct JOIN is a complete irrelevance with regard to the GROUP BY. 

-- So making the following change to the GROUP BY of the query ensures that 
-- each 'Alan Brick' gets his own row, even if they have the same sales_target.
 
SELECT	fname,	lname,	sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	                     ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(SELECT SUM(sales_target) 
	 FROM salesperson
	 HAVING SUM(sales_target) >
		(SELECT SUM(order_value)
		 FROM sale
		)
	)
GROUP BY SP.emp_no, fname, lname, sales_target
         --------*                 -- new column added       
HAVING sales_target > COALESCE(SUM(order_value),0)


-- The problem then is that potentially in the future the output might look like this:

fname           lname           sales_target   Sales achieved 
--------------- --------------- -------------- -------------- 
Alan            Brick           9.00           6
Alan            Brick           9.00           2
Billy           Custard         14.00          0
Chris           Digger          7.00           0
Dick            Ernst           11.00          0

-- but with no guarantee that the two 'Alan Brick' rows are anywhere near each other or even
-- on the same page of the report.
-- Question: How would your 'users' read the above query? Would they realize that the 2 rows
-- represent 2 different people?

-- Surely if SP.emp_no is in the GROUP BY, it should be in the SELECT list,

-- i.e.
SELECT	SP.emp_no, fname, lname, sales_target, COALESCE(SUM(order_value),0) 'Sales achieved'
        --------*                         -- column added
FROM 	salesperson SP LEFT JOIN sale S
	                     ON SP.emp_no = S.emp_no
WHERE 	EXISTS
	(SELECT SUM(sales_target) 
	 FROM salesperson
	 HAVING SUM(sales_target) >
		(SELECT SUM(order_value)
		 FROM sale
		)
	)
GROUP BY SP.emp_no, fname, lname, sales_target
         --------*
HAVING sales_target > COALESCE(SUM(order_value),0)

-- So the output would now be (with one Alan Brick)

emp_no      fname           lname           sales_target   Sales achieved 
----------- --------------- --------------- -------------- -------------- 
10          Alan            Brick           9.00           8
20          Billy           Custard         14.00          0
30          Chris           Digger          7.00           0
40          Dick            Ernst           11.00          0

-- and, if there were 2 Alan Bricks, then the output would show clearly
-- that they are 2 different people.

emp_no      fname           lname           sales_target   Sales achieved 
----------- --------------- --------------- -------------- -------------- 
10          Alan            Brick           9.00           6
20          Billy           Custard         14.00          0
30          Chris           Digger          7.00           0
40          Dick            Ernst           11.00          0
90          Alan            Brick           9.00           2

-- THE MORAL IS: "WRITE THE CORRECT 'GROUP BY' FIRST & THEN COPY/PASTE INTO THE 'SELECT' LIST"
-- NOT THE REVERSE as you might have surmised from earlier study/lectures.
   ***************


-- END OF 'MANDATORY PART OF PRACTICAL'

-- If you have time



--p6) Can you achieve the same output WITHOUT using EXISTS? Yes, with a small code change.
--    Hint: make the WHERE clause look like:

WHERE (subquery returning one number) >  (subquery returning another number)  

SELECT	SP.emp_no, fname, lname, sales_target,	COALESCE(SUM(order_value),0) 'Sales achieved'
FROM 	salesperson SP LEFT JOIN sale S
	ON SP.emp_no = S.emp_no
WHERE 	
	(SELECT sum(sales_target) FROM salesperson)
        >
	(SELECT sum(order_value) FROM sale)

GROUP BY SP.emp_no, fname, lname, sales_target
HAVING sales_target > COALESCE(SUM(order_value),0)


-- Everyone would agree that: 
-- This code is
-- a) simpler to read
-- b) simpler to write
-- c) easier to maintain
-- d) only works because these subqueries each return a 1 row 1 column 'atomic' number.
-- We have NOT broken the rule of "cannot use an aggregate in a WHERE clause";
--    we used it in a subquery of the WHERE clause, an important difference
-- In SQL, as in life, there are many ways of achieving the same end!

-- As a student your question might be "which will be faster?"
-- Answer: too many factors to determine easily, but if your DBMS has a 
-- 'show execution plan/costs' feature you could run the two together and see.

-- Last time we ran the code of p6) alongside the code of p5) (with EXISTS) the Execution plans 
-- in SQL SERVER 2000 were absolutely IDENTICAL and the costs of each were 50% of the batch! 

-- Later in the course we will see other examples of 'different' ways to achieve the same end.



-- p7) For your consideration / information.
--     How do we find the names associated with the top 2 sales_target(s)?

SELECT fname, lname, sales_target
FROM   salesperson
ORDER BY sales_target DESC

fname           lname           sales_target   
--------------- --------------- -------------- 
Billy           Custard         14.00
Fred            Goalie          13.00
Ernest          Flipper         12.00
Dick            Ernst           11.00
Alan            Brick           9.00
Chris           Digger          7.00

-- That does it. Just read the top 2 rows from the output of 6 rows,
-- but somewhat inefficient if you had 600/6000/60000 rows.






-- SELECTing the top few rows
-- ==========================

-- Very nice SQL Server option (since about 1997). 

SELECT TOP 2 	fname, lname, sales_target
FROM 		salesperson
ORDER BY	sales_target DESC

fname           lname           sales_target   
--------------- --------------- -------------- 
Billy           Custard         14.00
Fred            Goalie          13.00


-- But what if there was another person who had a target of 13, could I get them as well?
-- Yes, via 'with ties'.  
SELECT TOP 2 WITH TIES fname, lname, sales_target
FROM 		        salesperson
ORDER BY	        sales_target DESC

-- You can base your question on percentages as in:
SELECT TOP 35 PERCENT WITH TIES fname, lname, sales_target
FROM 		                salesperson
ORDER BY	                sales_target DESC

-- Oracle achieves similar functionality via 'inline VIEWs'.
SELECT  	ROWNUM AS Rank, fname, lname, sales_target
FROM 		(
		SELECT fname, lname, sales_target
		FROM salesperson
		ORDER BY sales_target DESC
		)                    -- first view of a subquery in a FROM clause! 
WHERE 		ROWNUM <= 2          -- 'ROWNUM' is a special pseudo column in Oracle 



/***************************************************************************************
**										      **
** 		END OF CHAPTER 9. Practical 2.			        	      **
**										      **
****************************************************************************************/



































/***************************************************************************************
**										      **
** 		CHAPTER 9. Practical 3.					  	      **
**		Correlated Subqueries (and how to work around them)	              ** 			
**										      **
****************************************************************************************/
SELECT * FROM dept
SELECT * FROM salesperson
SELECT * FROM sale
SELECT * FROM contact
SELECT * FROM company

-- Correlated subqueries are quite hard.
-- You will occasionally need them, but they can often be coded in another way
-- that you might consider easier, or maybe harder, - we will show you both.


-- Tutorial Portion. We would like you to step thru this at your own speed.
-- ================
--Part A) an introduction to a new sort of 'problem'
--Part B) a first stab might be 
--Part C) getting rid of the max/GROUP BY 
--Part D) enhancing the simplistic subquery   
--Part E) the fatal flaw, and proof
--Part F) moving towards the solution
--Part G) mandatory aliasing needed and solution achieved 
--Part H) tidying up the solution
--Part I) they are all the same: lets get the biggest per emp_no, or biggest per contact
--Part J) the ALTERNATIVE - a subquery in the FROM clause, 'an inline VIEW'!!

/************************************************************************************/
--Part A) an introduction to a new sort of 'problem'
/************************************************************************************/

--     Imagine we want to find the biggest sale per company.
--     It sounds easy.
--     Lets list the sales sorted by order_value within company_no first, so that
--     we can easily work it out manually and then try and build up some code.

SELECT company_no, order_value
FROM   sale
ORDER BY company_no, order_value

company_no  order_value 
----------- ----------- 
1000        5				--no
1000        7			--yes, biggest for company 1000

2000        3				--no
2000        12			--yes, biggest for company 2000

3000        3				--no
3000        6				--no
3000        27			--yes, .......3000

4000        2			--yes, .......4000

-- So the 2nd, 4th, 7th and 8th rows are the biggest per company.
-- The answer set we are trying to produce (via code) is therefore:

company_no  order_value 
----------- ----------- 
1000        7
2000        12
3000        27
4000        2


--    Seems easy enough, let's try.
 











/************************************************************************************/
--Part B) a first stab might be 
/************************************************************************************/

SELECT company_no, MAX(order_value) AS 'Biggest sale per company'
FROM sale
GROUP BY company_no

-- That produces :

company_no  Biggest sale per company 
----------- ------------------------ 
1000        7
2000        12
3000        27
4000        2

-- Problem solved, or a very minimalist answer?
-- We could easily JOIN to 'company' to pick up 'name' of company.
-- But what if we want to display 'date' of sale, 'emp_no' who made sale, 'description' ...?
-- Let's just add 'emp_no' to the SELECT list of last query
 
SELECT company_no, emp_no, MAX(order_value) AS 'Biggest sale per company'
FROM sale
GROUP BY company_no

-- gives:
-- "Column 'sale.emp_no' is invalid in the SELECT list because it is not contained 
-- in either an aggregate function or the GROUP BY clause." - SQL Server message.


-- We have broken a golden rule - 
-- "you must GROUP BY everything you SELECT that is not being aggregated".
-- Basically, 'emp_no' if 'SELECT'ed must be in Group by, so:

SELECT company_no, emp_no, MAX(order_value) AS 'Biggest sale per company per employee'
FROM sale
GROUP BY company_no, emp_no

-- gives:

company_no  emp_no      Biggest sale per company per employee 
----------- ----------- ------------------------------------- 
1000        10          5
2000        10          3
3000        50          27
1000        60          7
2000        60          12
3000        60          6
4000        60          2

-- and the column heading of the last column gives the game away.
-- We have "Changed the Question by changing the GROUP BY".

-- Our problem is really caused by
-- a) the max in the SELECT list meaning-
-- b) a 'GROUP BY' becomes necessary.












/************************************************************************************/
--Part C) getting rid of the max/GROUP BY 
/************************************************************************************/

--       What we need is a query that does
SELECT *

-- has no 'GROUP BY' clause, but with a 'WHERE' clause that manages to find just 4 of the 8 rows  
-- So let's start.
SELECT *
FROM sale
WHERE order_value = 

-- We know this is a good start because PRIMARILY the row is being SELECTed because
-- of the contents of the 'order_value' column and not anything else. 

-- The right hand side of the 'WHERE' clause must be some calculated 'Max',
-- so a subquery is needed.

SELECT *
FROM sale
WHERE order_value = (
		    SELECT MAX(order_value)
		    FROM sale
		    )
ORDER BY company_no

-- This runs, but produces the wrong answer:
order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System


-- the reason being:
-- the subquery produces a one number answer:
27

-- So we just get sales whose value is 27, i.e. the biggest of the whole population of 8   sales.









/************************************************************************************/
--Part D) enhancing the simplistic subquery   
/************************************************************************************/

-- We actually want (let�s just remind you of the original list):

company_no  order_value 
----------- ----------- 
1000        5
1000        7                 -- this row selected because it's value = biggest of rows 1 & 2

2000        3
2000        12                -- this row selected because it's value = biggest of rows 3 & 4

3000        3
3000        6
3000        27		      -- this row selected because it's value = biggest of rows 5, 6 & 7

4000        2		      -- this row selected because it's value = biggest of row 8 only

-- So let's make the subquery come back with 4 numbers namely 7, 12, 27, and 2.
-- These are the 4 maximums.
-- This is easily done by adding a 'GROUP BY' to the SUBQUERY:

SELECT *
FROM sale
WHERE order_value = (
		    SELECT MAX(order_value)
		    FROM sale
		    GROUP BY company_no         -- this line added
		    )
ORDER BY company_no

-- Let's just run the subquery alone.
SELECT MAX(order_value) AS biggest
FROM sale
GROUP BY company_no

-- Subquery produces:
biggest     
----------- 
7
12
27
2

-- If we now run the whole query we get
-- "Subquery returned more than 1 value. 
-- This is not permitted when the subquery follows 
-- =, !=, <, <= , >, >= or when the subquery is used as an expression".

-- Pretty self explanatory, so lets change the comparison operator '=' to be 'in'.

SELECT *
FROM sale
WHERE order_value IN   -- line altered from '=' to 'IN' 
		    (
		    SELECT MAX(order_value)
		    FROM sale
		    GROUP BY company_no         -- this line added
		    )
ORDER BY company_no

-- produces:
order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
100         60          1000        MM           7           2006-06-24 00:00:00.000                                Toshiba 6700 Pro
300         60          2000        OO           12          2006-07-14 00:00:00.000                                ScanPRO 4800 Scanner
600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System
500         60          4000        TT           2           2006-07-23 00:00:00.000                                Laser printer

-- Problem solved, or a fatal flaw in the logic?














/************************************************************************************/
--Part E) the fatal flaw, and proof
/************************************************************************************/

-- Fatal flaw I�m afraid! There�s a built in assumption that order_values are unique.
-- It assumes wrongly that there could NOT exist a sale whose order_value happened to be the biggest
-- to its company, but also at the same time equal to the 2nd biggest to another company or
-- equal to the 17th biggest to yet another company.

-- Temporarily INSERT this row:
-- A sale whose value is 12 to company 3000, and  
-- is equal to the biggest to a different company (company 2000).

INSERT INTO sale VALUES(900, 60, 3000, 'PP', 12, NULL, 'someitem')
(1 row(s) affected)

-- Now rerun current version.

SELECT *
FROM sale
WHERE order_value IN   -- line altered from '=' to 'IN' 
		    (
		    SELECT MAX(order_value)
		    FROM sale
		    GROUP BY company_no         -- this line added
		    )
ORDER BY company_no

-- It will give you
order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
100         60          1000        MM           7           2006-06-24 00:00:00.000                                Toshiba 6700 Pro
300         60          2000        OO           12          2006-07-14 00:00:00.000                                ScanPRO 4800 Scanner
600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System
900         60          3000        PP           12          NULL                                                   someitem
500         60          4000        TT           2           2006-07-23 00:00:00.000                                Laser printer

-- which includes order_no 900, which isn�t the biggest sale to company 3000.
-- With the current code any sale of value 2, 7, 12 or 27 will be selected regardless.
-- 
-- Let's leave the extra sale in the table for now, 
-- so that we can be certain later that we get the right solution.














/************************************************************************************/
--Part F) moving towards the solution
/************************************************************************************/

-- Where we went wrong was:
--   Introducing a GROUP BY clause to the subquery hence producing multiple values. 
-- We should have:
--   Introduced a WHERE clause that found the biggest of some rows, not the biggest of all 8. 
--   That WHERE clause has to be based on company_no.

-- So we are now at this point:

SELECT *
FROM sale
WHERE order_value =     -- safe as subquery will return one value only
		    (
		    SELECT MAX(order_value)
		    FROM sale
		    WHERE company_no = ??     -- this line added, no grouping!
		    )
ORDER BY company_no

-- If we change the '??' to say 1000 we will then get the biggest sale to company 1000  (1 row).
-- If we change the '??' to say 2000 we will then get the biggest sale to company 2000  (1 row).

-- But, we want both sales to appear together along with every other company_no.
-- So, as we can't hard code the value we better say company_no,
-- arriving at this point:

SELECT *
FROM sale
WHERE order_value =     -- safe as subquery will return one value only
		    (
		    SELECT MAX(order_value)
		    FROM sale
		    WHERE company_no = company_no
		    )
ORDER BY company_no

-- However, as you can imagine a line of code saying
WHERE company_no = company_no

-- says, "include in the calculation every row whose company_no column contains
--       the same data as it's company_no column", rather true for every row!!

-- The line
WHERE company_no = company_no

-- is about as useful as 
WHERE 7 = 7 and 65 = 65

-- But we are now very close.




















/************************************************************************************/
--Part G) mandatory aliasing needed and solution achieved 
/************************************************************************************/

-- Recognise this: There are 2 different browses of 'sale' happening and we need to keep 
-- taking/using the 'company_no' from the sale row being read by the outer query that is 
-- itself constantly changing as we browse the table.

-- The problem is the SQL parser will check and resolve column names (requiring that
-- they are unambiguous) against the NEAREST 'FROM' clause,
-- which for our dodgy line
WHERE company_no = company_no
-- means against the sale (browse) of the 'FROM' clause immediately above it (the inner   query).


-- The solution to the problem is: (this code runs and works)

SELECT *
FROM   sale  As OuterSaleBrowse     -- note definition of alias 
WHERE  order_value =     
		    (
		    SELECT MAX(order_value)
		    FROM   sale
		    WHERE  company_no = OuterSaleBrowse.company_no  -- note use of alias 
		    )
ORDER BY company_no

-- This produces
order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
100         60          1000        MM           7           2006-06-24 00:00:00.000                                Toshiba 6700 Pro
300         60          2000        OO           12          2006-07-14 00:00:00.000                                ScanPRO 4800 Scanner
600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System
500         60          4000        TT           2           2006-07-23 00:00:00.000                                Laser printer

-- Correct answer, hurrah! Even with that 9th sale of 12 to company_no 3000 in the data.

-- Note *****
-- The rules of algebra just went out the window, things in parentheses don't necessarily run 
-- 1st and just once! This subquery cannot run standalone.
-- It would say 
"The column prefix 'OuterSaleBrowse' does not match with a table name
 or alias name used in the query."

-- This is called a CORRELATED subquery, 
-- meaning "the subquery requires a value from above rather than (the usual) value from   below".

-- The table of the inner query DID NOT NEED TO BE ALIASED.
-- The table of the outer query MUST BE ALIASED.

-- However what everybody and every textbook does is alias both tables (both browses of the table):




/************************************************************************************/
--Part H) tidying up the solution
/************************************************************************************/

SELECT *
FROM sale  S1     -- simple alias 'S1' (required)
WHERE order_value =     
		    (
		    SELECT MAX(order_value)
		    FROM sale S2        -- simple alias, not needed but makes next line better
		    WHERE S2.company_no = S1.company_no  -- note use of both alias' 
		    )
ORDER BY company_no

order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
100         60          1000        MM           7           2006-06-24 00:00:00.000                                Toshiba 6700 Pro
300         60          2000        OO           12          2006-07-14 00:00:00.000                                ScanPRO 4800 Scanner
600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System
500         60          4000        TT           2           2006-07-23 00:00:00.000                                Laser printer















/************************************************************************************/
--Part I) they are all the same, lets get the biggest per emp_no, or biggest per contact
/************************************************************************************/


-- Once you have got the hang of that, they are all the same.
-- In 20 seconds flat you can copy/paste the query and change 
WHERE S2.company_no = S1.company_no 
-- to
WHERE S2.emp_no = S1.emp_no 
-- and change ORDER BY to be based on 'emp_no' and you have the biggest sale per employee

SELECT *
FROM sale  S1     -- simple alias 'S1' (required)
WHERE order_value =     
		    (
		    SELECT MAX(order_value)
		    FROM sale S2        -- simple alias, not needed but makes next line better
		    WHERE S2.emp_no = S1.emp_no  -- note use of both alias' 
		    )
ORDER BY emp_no

order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
400         10          1000        MM           5           2006-08-09 00:00:00.000                                Modems and Cables etc
600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System
300         60          2000        OO           12          2006-07-14 00:00:00.000                                ScanPRO 4800 Scanner
900         60          3000        PP           12          NULL                                                   someitem

-- Note, employee 60 has made 2 sales of 12 which are the Joint highest for that employee.

-- The following simple change finds the biggest sale per contact code.

SELECT *
FROM sale  S1     -- simple alias 'S1' (required)
WHERE order_value =     
		    (
		    SELECT MAX(order_value)
		    FROM sale S2        -- simple alias, not needed but makes next line better
		    WHERE S2.contact_code = S1.contact_code  -- note use of both alias' 
		    )
ORDER BY contact_code

-- They are all the same!!

-- So can we agree: "it runs the subquery once for each row of the outer query to generate
--                 "a value to then decide whether that row of the outer browse is SELECTed".













/************************************************************************************/
--Part J) the ALTERNATIVE - a subquery in the FROM clause !! 'an inline VIEW'
/************************************************************************************/

-- The process above might be inefficient if the table contained 10000 rows, 
-- meaning 10000 runs of the subquery??

-- Fortunately, the developers who write DBMSystems and Execution engines realise a short cut
-- can be made namely:

-- (Assuming we want biggest per company)
-- Generate a temporary summarised table containing one row for each company, say, holding just
-- the 'company_no' and the biggest 'order_value' for that company.
-- It would look like this:

company_no  biggest     
----------- ----------- 
1000        7
2000        12
3000        27
4000        2

-- They then JOIN the original 10000 row table to the summarised table on both columns:
-- The 'company_no' and the numeric 'order_value' to produce the result set.

-- You could do this yourself by 
-- 1) Creating (using CREATE Table) a Summary_Table.
-- 2) INSERTing into the Summary_Table the result of a SELECT that did a GROUP BY company_no.
-- 3) Then write your JOIN.

-- However, it is easier than that because all the major players allow you to put a subquery
-- into the 'FROM' clause, as what is called an inline (non-catalogued, not in the schema) VIEW.

-- These next two queries therefore achieve the same thing. The first one is correlated.

SELECT *                    
FROM sale  S1     
WHERE order_value =     
		    (		-- Standard correlated sub query
		    SELECT MAX(order_value)
		    FROM sale S2        
	            WHERE S2.company_no = S1.company_no
		    )
ORDER BY company_no

-- This one is not CORRELATED. It creates the temporary summarised table in the FROM!
SELECT *
FROM sale S JOIN (
		     SELECT company_no, MAX(order_value) AS 'biggest'
		     FROM sale
		     GROUP BY company_no 
		    )  SUMM           -- inline query must be aliased for 'on' to work 
	ON  S.company_no  = SUMM.company_no
	AND S.order_value = SUMM.biggest -- JOINing on both columns
ORDER BY S.company_no

-- It produces the correct answer, of course!
order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        company_no  biggest     
----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- ----------- ----------- 
100         60          1000        MM           7           2006-06-24 00:00:00.000                                Toshiba 6700 Pro                                   1000        7
300         60          2000        OO           12          2006-07-14 00:00:00.000                                ScanPRO 4800 Scanner                               2000        12
600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System                 3000        27
500         60          4000        TT           2           2006-07-23 00:00:00.000                                Laser printer                                      4000        2


/*****************************************************************
*								 *
-- End of Tutorial. You can try it with some Lab work NOW. 							 *
*								 *
*/****************************************************************



-- p1) Use either or both (preferably) techniques to display the biggest sale per dept_no.
-- One problem, 'sale' doesn�t have a 'dept_no' column, but knows someone who does. 
-- No need to access the table called dept!!

-- Either start from a 'blank page' or use a 'starter' template (page down to find it)
-- and 'fill in the gaps'.
























 







-- Starter Template for 'correlated version'

SELECT dept_no, S1.*        -- display dept_no to make sense of output                   
FROM sale  S1     
WHERE order_value =     
		    (		
		    SELECT MAX(order_value)
		    FROM sale S2        
             	    WHERE ?.dept_no = ?.dept_no      -- must be correlated on dept_no
		    )
ORDER BY dept_no

-- Soln code for 'correlated version'

SELECT dept_no, S1.*        -- display dept_no to make sense of output                   
FROM sale S1 JOIN salesperson SP1
	on S1.emp_no = SP1.emp_no     
WHERE order_value =     
		    (		
		    SELECT max(order_value)
		    FROM sale S2 JOIN salesperson SP2
		    	on S2.emp_no = SP2.emp_no     
              	    WHERE SP2.dept_no = SP1.dept_no      -- correlated on dept_no
		    )
ORDER BY dept_no

-- produces
dept_no     order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
1           400         10          1000        MM           5           2006-08-09 00:00:00.000                                Modems and Cables etc
3           600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System



-- Starter Template for 'inline VIEW version'

SELECT dept_no, S.*
FROM sale S 



JOIN   (	SELECT dept_no, MAX(order_value) AS 'biggest'
     	FROM sale 
     	GROUP BY dept_no 
     		      )  ??           -- inline query aliased  

	ON  ?.dept_no  = ?.dept_no
	AND S.order_value = ??.biggest   -- JOIN on both columns

ORDER BY dept_no


-- Soln code using 'inline VIEW'

SELECT SP.dept_no, S.*
FROM sale S JOIN salesperson SP
	ON S.emp_no = SP.emp_no
	JOIN 	      (
     			SELECT dept_no, MAX(order_value) AS 'biggest'
     			FROM sale S2 JOIN salesperson SP2
			ON S2.emp_no = SP2.emp_no
     			GROUP BY dept_no 
     		      )  SUMM           -- inline query aliased  
	ON  SP.dept_no  = SUMM.dept_no
	AND S.order_value = SUMM.biggest   -- JOIN on both columns
ORDER BY SP.dept_no




-- also produces this correct answer
dept_no     order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
1           400         10          1000        MM           5           2006-08-09 00:00:00.000                                Modems and Cables etc
3           600         50          3000        PP           27          2006-05-23 00:00:00.000                                Complete Desktop Publishing System



-- The 'inline VIEW' version effectively JOINs the following 2 tables on dept_no and value.
-- The output of the 1st 3 lines of outer query:
dept_no     order_no    emp_no      company_no  contact_code order_value order_date                                             description                                        
----------- ----------- ----------- ----------- ------------ ----------- ------------------------------------------------------ -------------------------------------------------- 
3           100         60          1000        MM           7           2006-06-24 00:00:00.000                                Toshiba 6700 Pro
3           200         60          3000        QQ           6           2006-05-01 00:00:00.000                                MS Office Professional * 30
3           300         60          2000        OO           12          2006-07-14 00:00:00.000                                ScanPRO 4800 Scanner
1 ***       400         10          1000        MM           5  ***      2006-08-09 00:00:00.000                                Modems and Cables etc
3           500         60          4000        TT           2           2006-07-23 00:00:00.000                                Laser printer
3 ***       600         50          3000        PP           27 ***      2006-05-23 00:00:00.000                                Complete Desktop Publishing System
1           700         10          2000        OO           3           2006-01-23 00:00:00.000                                SQL Server 2005 20 user licence
3           800         60          3000        RR           3           2006-11-15 00:00:00.000                                Printer cartridges
3           900         60          3000        PP           12          NULL                

-- The output of the subquery appearing in the 'FROM':
dept_no     biggest     
----------- ----------- 
1 ***       5  ***
3 ***       27 ***

-- They are JOINed on the rows WHERE the '***' shows the matching values.

-- If you have time:

--p2) You know more than one technique of finding depts with no people
--    The same techniques can be used to find people with no sales.
--    Quite easy because there is a direct link between the 2 tables involved.

--    But what if you wanted to find depts with no sales?
--    The tables have no common column.

--    Is this what we are trying to do?
--    "Put the dept row in the output if there doesn�t exist (sounds like SQL - NOT EXISTS) an
--    answer set(sounds like SELECT *) when we go looking for sales made by any person in 
--    that dept(dept_no)".

--    Earlier you learnt that a subquery that follows EXISTS or NOT EXISTS can
--    SELECT anything, doesn�t matter what, because it is a TRUE or FALSE that is 'returned'.
--    But that was at a time when you believed all subqueries ran just once.
--    We now know that is NOT the case if the subquery is correlated.

--    So it is for example TRUE 
--    "that there does not exist an answer set when you search for sales by an 
--    emp_no whose dept_no is 2".
--    It is NOT TRUE (FALSE) 
--    "that there does not exist an answer set when you search for sales by an
--    emp_no whose dept_no is 1".

--    Convert this into a correlated NOT EXISTS.

SELECT * 		-- show me the details of
FROM dept D		-- a dept (known as D)			
WHERE NOT EXISTS        -- 'if there is'nt
	(
	SELECT *           -- an answer set
	FROM sale	   -- when i look for sales
	WHERE dept_no = ?  -- whose salesperson's dept_no is equal the dept_no of the dept(D)
	)

-- Solution

SELECT * 		
FROM dept D		
WHERE NOT EXISTS        
	(
	SELECT *           
	FROM sale S JOIN salesperson SP
		ON S.emp_no = SP.emp_no
	WHERE SP.dept_no = D.dept_no
	)


-- Output: 
dept_no     dept_name            manager              sales_target   
----------- -------------------- -------------------- -------------- 
2           Business Systems     Barbara Banana       20.00
4           Desktop Systems      Diver Dan            10.00


-- p3) But surely this next non-correlated alternative is much easier
--        a) to read
--        b) to write
--        c) to maintain

SELECT * 		
FROM dept 		
WHERE dept_no NOT IN           -- whose dept_no is'nt one of the dept_no's
	(
	SELECT dept_no         -- of the people who have sold
	FROM sale S JOIN salesperson SP
		      ON S.emp_no = SP.emp_no
	)          				-- not correlated 

-- The only problem is if you run this UPDATE it all goes wrong.
UPDATE salesperson 
SET dept_no = NULL
WHERE emp_no = 10

-- Because the subquery
SELECT dept_no         
FROM sale S JOIN salesperson SP
	         ON S.emp_no = SP.emp_no
-- now produces a result set of
dept_no     
----------- 
3
3
3
NULL            -- a sale made by salesperson 10
3
3
NULL		-- a sale made by salesperson 10
3
3

-- Question. Is the value '1' as in 'dept_no = 1' NOT IN that result set?
-- You have 3 choices:
-- a) TRUE      		(it is NOT IN)
-- b) FALSE     		(it is IN)
-- c) not sure/unknown/NULL     (I am not sure if 1 is NOT IN)

-- The answer(unfortunately) is c).
-- So this query
SELECT * 		
FROM dept 		
WHERE dept_no NOT IN           -- whose dept_no is'nt one of the dept_no's
	(
	SELECT dept_no         -- of the people who have sold
	FROM sale S JOIN salesperson SP
		      ON S.emp_no = SP.emp_no
	)
-- produces this
(0 row(s) affected)
-- If any of the dept_nos of the salespeople who have sold is NULL. 


-- Note: If you were to add a suitable 'WHERE' to the subquery it will work again,
-- but would you remember.
SELECT * 		
FROM dept 		
WHERE dept_no NOT IN           -- whose dept_no is'nt one of the dept_no's
	(
	SELECT dept_no         -- of the people who have sold
	FROM sale S JOIN salesperson SP
		      ON S.emp_no = SP.emp_no
	WHERE SP.dept_no IS NOT NULL   -- but ignoring sales made by a person not in any dept
	)

-- Maybe the decision in the future is to set the emp_no of the salesperson 
-- to NULL on sale rows that he was responsible for if he leaves the company 
-- and we remove his row from salesperson.
-- (Foreign Key constraints can actually automate this via 'ON DELETE SET NULL')
-- A more sensible option of course would be to set his employment_status column to 'L'
-- meaning "has 'L'eft" the company.

-- So just in case salesperson is ever going to be NULL in the future
-- you should recognise it is safer to use the NOT EXISTS which still returns TRUE if one 
-- of the dept_no's is NULL.

SELECT * 		
FROM dept D		
WHERE NOT EXISTS        
	(
	SELECT *           
	FROM sale S JOIN salesperson SP
		      ON S.emp_no = SP.emp_no
	WHERE SP.dept_no = D.dept_no
	)
-- produces those depts with no sales now that emp_no 10 (originally in dept_no 1)
-- has left dept_no=1 and moved to dept_no=NULL.

dept_no     dept_name            manager              sales_target   
----------- -------------------- -------------------- -------------- 
1           Animal Products      Adam Apricot         10.00
2           Business Systems     Barbara Banana       20.00
4           Desktop Systems      Diver Dan            10.00


-- clean up
UPDATE salesperson 
SET dept_no = 1
WHERE emp_no = 10

DELETE FROM sale WHERE order_no = 900

/***************************************************************************************
**										      **
** 		END OF CHAPTER 9. Practical 3.				              **
**										      **
****************************************************************************************/



/***************************************************************************************
**										      **
-- Extra examples of correlated code, feel free to read and execute					              **
**										      **
****************************************************************************************/



SELECT fname + ' ' + lname as name, sales_target, 
       CAST (sales_target / (SELECT SUM(sales_target) FROM salesperson) * 100 AS DECIMAL(3,1)) AS percent_of_total
FROM salesperson

-- add a column for bonus
ALTER table salesperson
ADD bonus_factor decimal(5,2)

--show their bonus factors based on sales vs target
SELECT fname + ' ' + lname as name, 
       sales_target, 
       COALESCE(sum(order_value),0) as total_sales, 
       COALESCE(sum(order_value),0)/ sales_target * 100 as bonus_factor,
       round(COALESCE(sum(order_value),0)/ sales_target * 100,0) as rounded_bonus_factor
FROM salesperson sp LEFT JOIN sale s
ON sp.emp_no = s.emp_no
GROUP BY fname + ' ' + lname, sales_target

-- give them their bonus factor
UPDATE salesperson 
SET bonus_factor = round( (SELECT sum(order_value) FROM sale S WHERE S.emp_no = SP.emp_no)
		 / sales_target * 100,0)
FROM salesperson AS SP -- SQL Server extension (extra FROM clause)

-- show result of UPDATE
SELECT emp_no, sales_target, bonus_factor
FROM salesperson

UPDATE salesperson
SET bonus_factor = 0

--remove the added column
ALTER TABLE salesperson
	DROP column bonus_factor

-- remove salespeople who have missed target!!
DELETE FROM salesperson 
FROM salesperson SP          -- SQL Server extension DELETE FROM FROM
WHERE sales_target > (SELECT COALESCE(SUM(order_value),0) FROM sale
			WHERE emp_no = SP.emp_no) * 2 

-- only 3 left
SELECT *
FROM salesperson

-- put the 3 salespeople back
INSERT  into salesperson values( 20, 'Billy', 	'Custard', 	2, 14,	'Hampshire', 	'RF3 9UD', '(0306)7871',        'Bright Prospect' )
INSERT  into salesperson values( 30, 'Chris', 	'Digger', 	2,  7, 	'Hampshire',	'W45 TY3', '(0908)922211', 	'Should be promoted within the next 4 months' )
INSERT  into salesperson values( 40, 'Dick', 	'Ernst', 	3, 11, 	'London', 	NULL, 	   '0207-898-9656', 	NULL )









































/***************************************************************************************
**										      **
** 		CHAPTER 10. Practical 1.				  	      **
**		Outer JOINs revisited, Self JOINs, Unions			      **
**										      **
****************************************************************************************/

SELECT * FROM dept
SELECT * FROM salesperson
SELECT * FROM sale
SELECT * FROM contact
SELECT * FROM company


--p1) We want to produce one list comprising three things all in one list
--	a) Manager names
--      b) Sales people's firstname and surname concatenated 
--      c) All Contacts names but only include those we have not sold to
--    The 1st column should be headed 'Name'
--    A second column called 'Type' should contain an 'm' or 'p' or 'c' as appropriate
--    to indicate manager/person/contact respectively
--    Sort the result set based on the second (type) column

SELECT 	manager 'Name', 'm' AS Type
FROM 	dept
UNION
SELECT 	fname + ' ' + lname, 'p'
FROM 	salesperson 
UNION
SELECT	name, 'c'
FROM	contact C LEFT JOIN sale S
ON 	C.company_no = S.company_no
AND	C.contact_code = S.contact_code
WHERE 	S.company_no IS NULL 
ORDER BY Type

-- produces
Name                            Type 
------------------------------- ---- 
Marvellous Marvin               c
Naughty Nick                    c
Ricky Rambo                     c
Uppy Umbrella                   c
Adam Apricot                    m
Barbara Banana                  m
Diver Dan                       m
Paul Peach                      m
Alan Brick                      p
Billy Custard                   p
Chris Digger                    p
Dick Ernst                      p
Ernest Flipper                  p
Fred Goalie                     p

(14 row(s) affected)


--p2) SELF JOINS
--    We want for some reason to know companies that are in the same county.
--    The following code solves the problem?, simply displaying them vertically ordered

SELECT county, name
FROM company
ORDER BY county

-- produces

county          name                 
--------------- -------------------- 
Devon           Kipper Kickers Inc
London          Judo Jeans PLC
London          Happy Heaters PLC
London          Icicle Igloos Inc

-- As there will be 'many' companies and there are relatively few counties
-- companies just can't help being in the same county as an other company(s)
-- But what if it was post_code matches we were looking for
-- A post code is one side of a street so with 10000 customers, there might be
-- 9950+ post_codes
-- The companies that share a post_code would be next to each other but hidden 
-- in the midst of a large list

-- This is where a self JOIN might be helpful

SELECT C1.county, C1.name, C2.name
FROM company C1 JOIN company C2
	on C1.county = C2.county            -- don't JOIN on company_no (anything but)

-- Can you picture what this would produce?

-- Try it





county          name                 name                 
--------------- -------------------- -------------------- 
London          Happy Heaters PLC    Happy Heaters PLC
London          Icicle Igloos Inc    Happy Heaters PLC
London          Judo Jeans PLC       Happy Heaters PLC
London          Happy Heaters PLC    Icicle Igloos Inc
London          Icicle Igloos Inc    Icicle Igloos Inc
London          Judo Jeans PLC       Icicle Igloos Inc
London          Happy Heaters PLC    Judo Jeans PLC
London          Icicle Igloos Inc    Judo Jeans PLC
London          Judo Jeans PLC       Judo Jeans PLC
Devon           Kipper Kickers Inc   Kipper Kickers Inc

-- each row JOINs to itself 'Happy' 'Happy' & 'Kipper' 'Kipper'
-- plus 'Happy' JOINs to 'Icicle' and 'Icicle' JOINs to 'Happy'

-- we can get rid of the 'Happy' & 'Happy' lines
-- by introducing a 'WHERE' clause

SELECT C1.county, C1.name, C2.name
FROM company as C1 JOIN company as C2
	on C1.county = C2.county 
WHERE C1.company_no <> C2.company_no           -- notice the <>

-- produces

county          name                 name                 
--------------- -------------------- -------------------- 
London          Icicle Igloos Inc    Happy Heaters PLC
London          Judo Jeans PLC       Happy Heaters PLC
London          Happy Heaters PLC    Icicle Igloos Inc
London          Judo Jeans PLC       Icicle Igloos Inc
London          Happy Heaters PLC    Judo Jeans PLC
London          Icicle Igloos Inc    Judo Jeans PLC

-- now, can we make either the 'Happy' 'Icicle' row appear or 'Icicle' 'Happy' but not both?
-- yes, easy, change the '<>' to a '<' or a '>' (does'nt matter which')
-- so
SELECT C1.county, C1.name, C2.name
FROM company C1 JOIN company C2
	on C1.county = C2.county 
WHERE C1.company_no < C2.company_no

-- produces

county          name                 name                 
--------------- -------------------- -------------------- 
London          Happy Heaters PLC    Icicle Igloos Inc
London          Happy Heaters PLC    Judo Jeans PLC
London          Icicle Igloos Inc    Judo Jeans PLC

-- but that is as good as it gets!

-- you could JOIN company C1 to company C2 to company C3
-- and it would be possible to display all 3 companies 'Happy' 'Icicle' & 'Judo'
-- on the same line

-- but, if there were 4 in the same county it would then take 4 rows to tell you
-- if there were 5 in the same county it would give you 20 different combinations of 3!
-- 8 in the same county would give you 6720 combinations if displayed 3-up

-- surely we can do better, rest assured we can
-- an alternative to self JOINs is to list them vertically in sequence but only include
-- the rows that have a county that occurs more than once in the whole population

-- write a query that instead of listing these 4 rows
county          name                 
--------------- -------------------- 
Devon           Kipper Kickers Inc
London          Judo Jeans PLC
London          Happy Heaters PLC
London          Icicle Igloos Inc

-- just lists these 3 rows (leaving out the Devon row as Devon only appears once)
county          name                 
--------------- -------------------- 
London          Judo Jeans PLC
London          Happy Heaters PLC
London          Icicle Igloos Inc
 

-- Here is a starter for you

SELECT *
FROM company
WHERE county ?

COUNT(*) > 1

-- Soln 

SELECT *
FROM company
WHERE county IN 
	(
	SELECT county
	FROM company
	GROUP BY county
	HAVING COUNT(*) > 1
	)


-- produces 

company_no  name                 tel             county          post_code  
----------- -------------------- --------------- --------------- ---------- 
1000        Happy Heaters PLC    (01306)345672   London          SE3 89L   
2000        Icicle Igloos Inc    0207-987-1265   London          N1 4LH    
3000        Judo Jeans PLC       0207-478-2990   London          N9 2FG    





-- easy when you see it but few delegates crack that one
-- the principle is:
--    outer query is a list not a summary, so no GROUP BY can't use HAVING
--    but SELECTs rows based on criteria that searches into another result set
--    that is Grouped, does Count's and can use HAVING

-- Of course now you are an expert on inline VIEWs you could put your subquery 
-- in the FROM and JOIN to it!
-- So it becomes not JOIN 'me' to 'me' but JOIN 'me' to a 'summarised/filtered version of me'!  
SELECT *
FROM company C JOIN (
		    SELECT county
		    FROM company
		    GROUP BY county
		    HAVING COUNT(*) > 1
		    ) GROUPED
ON C.county = GROUPED.county





-- Perhaps we have done enough SQL for one course now

/***************************************************************************************
**										      **
** 		END OF CHAPTER 10. Practical 1				       	      **
**										      **
****************************************************************************************/





































/***************************************************************************************
**										      **
** 		CHAPTER 10. Practical 2.				  	      **
**		Reading SQL				 			      **
**										      **
****************************************************************************************/
--  Read, execute if you wish, and type in in English what each piece
--  of SQL does.


--p1) Depts that have hit target 

SELECT 	D.dept_no, dept_name
FROM 	dept D JOIN salesperson SP
	ON D.dept_no = SP.dept_no
JOIN 	sale S
	ON SP.emp_no = S.emp_no
GROUP BY 
	D.dept_no, dept_name, D.sales_target 
HAVING	D.sales_target <= SUM(order_value)



--p2) Depts that have not hit target, but have sold something.

SELECT 	D.dept_no, dept_name
FROM 	dept D JOIN salesperson SP
	ON D.dept_no = SP.dept_no
JOIN 	sale S
	ON SP.emp_no = S.emp_no
GROUP BY 
	D.dept_no, dept_name, D.sales_target 
HAVING	D.sales_target > SUM(order_value)



--p3) Depts that have not hit target, but have either sold something 
--    or have sales people in who have not sold at all, but does not 
--    include any depts that have no sales people at all.

SELECT 	D.dept_no, dept_name
FROM 	dept D JOIN salesperson SP
	ON D.dept_no = SP.dept_no
LEFT JOIN sale S
	ON SP.emp_no = S.emp_no
GROUP BY 
	D.dept_no, dept_name, D.sales_target 
HAVING	D.sales_target >= SUM(order_value)
OR 	SUM(order_value) IS NULL



--p4) All depts that have not hit target, regardless of whether they
--    have either sold anything or whether they have sales people in them.

SELECT 	D.dept_no, dept_name
FROM 	dept D LEFT JOIN salesperson SP
	ON D.dept_no = SP.dept_no
LEFT JOIN sale S
	ON SP.emp_no = S.emp_no
GROUP BY 
	D.dept_no, dept_name, D.sales_target 
HAVING	D.sales_target >= SUM(order_value)
OR 	SUM(order_value) IS NULL




--p5) Sales people who have sold to all companies - Fred Goalie.
--    "List sales people if the count of different companies they have sold 
--    to is equal to the number of companies in the company table".

SELECT 	SP.emp_no, fname, lname
FROM 	sale S JOIN salesperson SP
	ON S.emp_no = SP.emp_no
Group by 
	SP.emp_no, fname, lname
HAVING 	COUNT(DISTINCT company_no) = 
	(
	SELECT COUNT(*)
	FROM company
	)


--p6) Sales people who have sold to all companies (again!) - Fred Goalie.
--    "List sales people if there isn't (NOT EXISTS) a company that isn't
--    represented (NOT EXISTS) in their sales. The 2nd subquery is trying 
--    to find 'is there an answer set when you look for sales of one person
--    to one company?'"	

SELECT 	emp_no, fname, lname
FROM 	salesperson SP
WHERE 	NOT EXISTS
	(
	SELECT 	*
	FROM 	company C
	WHERE 	NOT EXISTS
		(
		SELECT 	* 
		FROM 	sale S
		WHERE 	S.company_no 	= C.company_no     --correlated on company_no	
		AND 	S.emp_no 	= SP.emp_no  --correlated on emp_no
		) 
	)





--p7) Names of depts who have employees who have not hit target but
--    have sold something. 

SELECT 	DISTINCT D.dept_no ,dept_name 
FROM 	salesperson SP JOIN sale S
	ON SP.emp_no = S.emp_no
	JOIN dept D
	ON D.dept_no = SP.dept_no
GROUP BY D.dept_no, dept_name, SP.emp_no, SP.sales_target 
HAVING 	SUM(order_value) < SP.sales_target


--p8) Names of depts who have someone who has not hit target. 
--    Includes those who have not sold at all.

SELECT 	DISTINCT D.dept_no ,dept_name 
FROM 	salesperson SP LEFT JOIN sale S
	ON SP.emp_no = S.emp_no
	JOIN dept D
	ON D.dept_no = SP.dept_no
GROUP BY 
	D.dept_no, dept_name, SP.emp_no, SP.sales_target 
HAVING 	SUM(order_value) < SP.sales_target
OR 	SUM(order_value) IS NULL 



/***************************************************************************************
**										      **
** 		END OF Chapter 10. Practical 2.				              **
**										      **
****************************************************************************************/
