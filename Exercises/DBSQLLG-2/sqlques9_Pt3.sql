/***************************************************************************************
**										      **
** 		CHAPTER 9. Practical 3.					  	      **
**		Correlated Subqueries (and how to work around them)	              ** 			
**										      **
****************************************************************************************/
SELECT * FROM dept
SELECT * FROM salesperson
SELECT * FROM sale
SELECT * FROM contact
SELECT * FROM company

-- Correlated subqueries are quite hard.
-- You will occasionally need them, but they can often be coded in another way
-- that you might consider easier, or maybe harder, - we will show you both.


\	
/***************************************************************************************
**										      **
** 		END OF CHAPTER 9. Practical 3.				              **
**										      **
****************************************************************************************/



/***************************************************************************************
**										      **
-- Extra examples of correlated code, feel free to read and execute					              **
**										      **
****************************************************************************************/



SELECT fname + ' ' + lname as name, sales_target, 
       CAST (sales_target / (SELECT SUM(sales_target) FROM salesperson) * 100 AS DECIMAL(3,1)) AS percent_of_total
FROM salesperson

-- add a column for bonus
ALTER table salesperson
ADD bonus_factor decimal(5,2)

--show their bonus factors based on sales vs target
SELECT fname + ' ' + lname as name, 
       sales_target, 
       COALESCE(sum(order_value),0) as total_sales, 
       COALESCE(sum(order_value),0)/ sales_target * 100 as bonus_factor,
       round(COALESCE(sum(order_value),0)/ sales_target * 100,0) as rounded_bonus_factor
FROM salesperson sp LEFT JOIN sale s
ON sp.emp_no = s.emp_no
GROUP BY fname + ' ' + lname, sales_target

-- give them their bonus factor
UPDATE salesperson 
SET bonus_factor = round( (SELECT sum(order_value) FROM sale S WHERE S.emp_no = SP.emp_no)
		 / sales_target * 100,0)
FROM salesperson AS SP -- SQL Server extension (extra FROM clause)

-- show result of UPDATE
SELECT emp_no, sales_target, bonus_factor
FROM salesperson

UPDATE salesperson
SET bonus_factor = 0

--remove the added column
ALTER TABLE salesperson
	DROP column bonus_factor

-- remove salespeople who have missed target!!
DELETE FROM salesperson 
FROM salesperson SP          -- SQL Server extension DELETE FROM FROM
WHERE sales_target > (SELECT COALESCE(SUM(order_value),0) FROM sale
			WHERE emp_no = SP.emp_no) * 2 

-- only 3 left
SELECT *
FROM salesperson

-- put the 3 salespeople back
INSERT  into salesperson values( 20, 'Billy', 	'Custard', 	2, 14,	'Hampshire', 	'RF3 9UD', '(0306)7871',        'Bright Prospect' )
INSERT  into salesperson values( 30, 'Chris', 	'Digger', 	2,  7, 	'Hampshire',	'W45 TY3', '(0908)922211', 	'Should be promoted within the next 4 months' )
INSERT  into salesperson values( 40, 'Dick', 	'Ernst', 	3, 11, 	'London', 	NULL, 	   '0207-898-9656', 	NULL )


